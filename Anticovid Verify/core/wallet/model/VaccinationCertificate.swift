// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  VaccinationCertificate.swift
//  TousAntiCovid
//
//

import Foundation
import OrderedCollections

final class VaccinationCertificate: WalletCertificate {

    enum FieldName: String, CaseIterable {
        case authority
        case certificateId
        case issueDate
        case creationDate
        case country
        case name = "L0"
        case firstName = "L1"
        case birthDate = "L2" // Can be a lunar date.
        case diseaseName = "L3"
        case prophylacticAgent = "L4"
        case vaccineName = "L5"
        case vaccineMaker = "L6"
        case lastVaccinationStateRank = "L7"
        case completeCycleDosesCount = "L8"
        case lastVaccinationDate = "L9"
        case vaccinationCycleState = "LA"
    }
    
    override var message: Data? { value.components(separatedBy: WalletConstant.Separator.unit.ascii).first?.data(using: .ascii) }
    override var signature: Data? {
        guard let signatureString = value.components(separatedBy: WalletConstant.Separator.unit.ascii).last else { return nil }
        do {
            return try signatureString.decodeBase32(padded: signatureString.hasSuffix("="))
        } catch {
            return nil
        }
    }
    override var isSignatureAlreadyEncoded: Bool { false }
    var documentVersion : String?
    var firstName: String? { fields[FieldName.firstName.rawValue]?.replacingOccurrences(of: "/", with: ",") }
    var name: String? { fields[FieldName.name.rawValue] }
    var birthDateString: String?

    var diseaseName: String? { fields[FieldName.diseaseName.rawValue] }
    var prophylacticAgent: String? { fields[FieldName.prophylacticAgent.rawValue] }
    var vaccineName: String? { fields[FieldName.vaccineName.rawValue] }
    var vaccineMaker: String? { fields[FieldName.vaccineMaker.rawValue] }
    var lastVaccinationStateRank: String? { fields[FieldName.lastVaccinationStateRank.rawValue] }
    var completeCycleDosesCount: String? { fields[FieldName.completeCycleDosesCount.rawValue] }
    
    var lastVaccinationDate: Date?
    var lastVaccinationDateString: String? { lastVaccinationDate?.shortDateFormatted() }
    var sha256: String?
    var vaccinationCycleState: String? {
        guard let cycleState = fields[FieldName.vaccinationCycleState.rawValue] else { return nil }
        return cycleState
    }
    
    override var timestamp: Double { lastVaccinationDate?.timeIntervalSince1970 ?? 0.0 }
    
    override var pillTitles: [String] { ["wallet.proof.vaccinationCertificate.pillTitle".localized, vaccinationCycleState].compactMap { $0 } }
    override var shortDescription: String { [firstName, name].compactMap { $0 }.joined(separator: " ") }
    override var fullDescription: String {
        var text: String = "wallet.proof.vaccinationCertificate.description".localized
        text = text.replacingOccurrences(of: "<\(FieldName.name.rawValue)>", with: firstName ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.firstName.rawValue)>", with: name ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.birthDate.rawValue)>", with: birthDateString ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.diseaseName.rawValue)>", with: diseaseName ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.prophylacticAgent.rawValue)>", with: prophylacticAgent ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.vaccineName.rawValue)>", with: vaccineName ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.vaccineMaker.rawValue)>", with: vaccineMaker ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.lastVaccinationStateRank.rawValue)>", with: lastVaccinationStateRank ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.completeCycleDosesCount.rawValue)>", with: completeCycleDosesCount ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.lastVaccinationDate.rawValue)>", with: lastVaccinationDateString ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.vaccinationCycleState.rawValue)>", with: vaccinationCycleState ?? "N/A")
        return text
    }
    
    override init(id: String = UUID().uuidString, value: String, type: WalletConstant.CertificateType, needParsing: Bool = false) {
        super.init(id: id, value: value, type: type, needParsing: needParsing)
        guard needParsing else { return }
        self.birthDateString = parseBirthDate()
        self.lastVaccinationDate = parseLastVaccinationDate()
    }

    override func parse(_ value: String) -> [String: String] {
        var captures: [String: String] = [:]
        guard let regex = try? NSRegularExpression(pattern: type.validationRegex) else { return captures }
        let matches: [NSTextCheckingResult] = regex.matches(in: value, options: [], range: NSRange(location: 0, length: value.count))
        guard let match = matches.first else { return captures }
        
        FieldName.allCases.forEach {
            let matchRange: NSRange = match.range(withName: $0.rawValue)
            guard let substringRange = Range(matchRange, in: value) else { return }
            let capture = String(value[substringRange])
            switch $0 {
            case .authority:
                authority = capture
            case .certificateId:
                certificateId = capture
            case .issueDate:
                issueDate = capture
            case .creationDate:
                creationDate = capture
            case .country:
                country = capture
            default:
                captures[$0.rawValue] = capture
            }
        }
        let separator = "\u{001F}"
        var components = value.components(separatedBy: separator)
        components = components.dropLast()
        let output = components.joined(separator: separator)
        sha256 = output.sha256?.hexString
        return captures
    }
    
    private func parseBirthDate() -> String? {
        guard let birthDateString = fields[FieldName.birthDate.rawValue], birthDateString.count == 8 else { return nil }
        let dayString: String = birthDateString[0...1]
        let monthString: String = birthDateString[2...3]
        let yearString: String = birthDateString[4...7]
        return "\(dayString)/\(monthString)/\(yearString)"
    }
    
    private func parseLastVaccinationDate() -> Date? {
        guard let analysisDateString = fields[FieldName.lastVaccinationDate.rawValue], analysisDateString.count == 8 else { return nil }
        let dayString: String = analysisDateString[0...1]
        let monthString: String = analysisDateString[2...3]
        let yearString: String = analysisDateString[4...7]
        let dateComponents: DateComponents = DateComponents(year: Int(yearString), month: Int(monthString), day: Int(dayString))
        return Calendar.current.date(from: dateComponents)
    }
}

extension VaccinationCertificate: BarcodeProtocolObject {
    func getUIInformation(result: CertificateState, shouldShowFullResult: Bool) -> OrderedDictionary<String, [FieldRow]> {
        var results : [FieldRow] = []
        if shouldShowFullResult {
            results.append(FieldRow.Field(title: "Etat du cycle de vaccination", subtitle: self.vaccinationCycleState == "TE" ? "Terminé" : "En cours",picto: ResultPicto.none))
            if let firstName = firstName, !firstName.isEmpty {
                results.append(FieldRow.Field(title: "Liste des prénoms du patient", subtitle: firstName,picto: .eye))
            }
            if let name = name, !name.isEmpty {
                results.append(FieldRow.Field(title: "Nom de famille du patient", subtitle: name,picto: .eye))
            }
            results.append(FieldRow.Field(title:"Date de naissance du patient", subtitle: self.birthDateString ?? "",picto: .eye))
            results.append(FieldRow.Field(title: "Nom de la maladie couverte", subtitle: self.diseaseName ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Agent prophylactique", subtitle: self.prophylacticAgent ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Nom du vaccin", subtitle: self.vaccineName ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Fabriquant du vaccin", subtitle: self.vaccineMaker ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Rang du dernier état de vaccination effectué", subtitle: self.lastVaccinationStateRank ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Nombre de doses attendues pour un cycle complet", subtitle: self.completeCycleDosesCount ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Date du dernier état du cycle de vaccination", subtitle: self.lastVaccinationDateString ?? "",picto: ResultPicto.none))
            
            results.append(FieldRow.Field(title: "Type de document", subtitle: "Attestation Vaccinale",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Date d’émission du document", subtitle: self.getIssueDate(),picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Date de création de la signature", subtitle: self.getCreationDate(),picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Identifiant de l’autorité de certification", subtitle: self.authority ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Identifiant du certificat", subtitle: self.certificateId ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Pays émetteur du document", subtitle: Formatter.shared.countryName(from: self.country ?? ""),picto: ResultPicto.none))
           
            let resultRow = FieldRow.Field(title: "DURÉE DEPUIS LA DERNIÈRE INJECTION", subtitle: self.lastVaccinationDate?.formatForVaccinOT() ?? "?", picto: ResultPicto.none)
    
            return ["Données de validité": [resultRow], "Informations du 2D-DOC": results]
        } else {

            if let firstName = firstName, !firstName.isEmpty {
                results.append(FieldRow.Field(title: "Liste des prénoms du patient", subtitle: firstName.uppercased(),picto: .eye))
            }
            if let name = name, !name.isEmpty {
                results.append(FieldRow.Field(title: "Nom de famille du patient", subtitle: name.uppercased(),picto: .eye))
            }
            results.append(FieldRow.Field(title: "Date de naissance du patient", subtitle: self.birthDateString ?? "",picto: .eye))
            
            return ["Informations du 2D-DOC": results]
        }
    }
    
    func getIdHash() -> String {
        return sha256 ?? ""
    }
    
}
