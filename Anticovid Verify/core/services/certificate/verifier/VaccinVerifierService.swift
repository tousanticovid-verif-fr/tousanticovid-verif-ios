//
//  VaccinVerifier.swift
//  Anticovid Verify
//
//

import Foundation

class VaccinVerifierService {
    
    static let shared = VaccinVerifierService()
    
    private let statService: StatService
    
    private let userData: UserDataManager
    
    let premiumManager: PremiumManager
    
    private let rulesMotorService: RulesService
    
    init(statService: StatService = .shared,
         userData: UserDataManager = .sharedInstance,
         rulesMotorService: RulesService = .shared,
         premiumManager: PremiumManager = .shared) {
        self.statService = .shared
        self.userData = userData
        self.rulesMotorService = rulesMotorService
        self.premiumManager = premiumManager
    }

    func verifyVaccin(certificate: VaccinationCertificate) -> VerifierResult? {
        return premiumManager.modeSelected == .tacVerif ? verifyLiteVaccin(certificate: certificate) : verifyOTVaccin(certificate: certificate)
    }

    private func verifyOTVaccin(certificate: VaccinationCertificate) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil
        
        // Verifier le Nom du vaccin et le nombre d'injection
        guard certificate.lastVaccinationDate != nil else {
            return nil
        }

        let isFinished = certificate.vaccinationCycleState?.lowercased() == "te"
        let validity = WalletManager.shared.checkCertificateSignature(certificate)
        
        if !validity {
            resultState = .notAuthentic
            globalValidity = .invalid
        } else if isFinished  {
            resultState = .vaccinalCycleComplet
            globalValidity = .to_decide
        } else {
            resultState = .inprogress
            globalValidity = .invalid
        }
        
        let controlType = premiumManager.modeSelected == .OT ?
        ConstStat.TACV_2DDOC_IOS_OT :
        ConstStat.TACV_2DDOC_IOS_PLUS
        
        return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: controlType, ressourceType:  ConstStat.D_DOC_L1), certHash: certificate.getIdHash())
    }

    private func verifyLiteVaccin(certificate: VaccinationCertificate) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil

        // Verifier le Nom du vaccin et le nombre d'injection
        guard let lastStateDate = certificate.lastVaccinationDate else {
            return nil
        }

        let isJanssen = (certificate.vaccineName?.value.lowercased() ?? "").contains("janssen")
        let isFinished = certificate.vaccinationCycleState?.lowercased() == "te"
        let validity = WalletManager.shared.checkCertificateSignature(certificate)

        let today = Date()
        let vaccinDelay = self.rulesMotorService.getVaccineDelay()
        
        let vaccineDelay = lastStateDate.dateByAddingDays(vaccinDelay.vaccineDelay)
        let vaccineDelayMax = lastStateDate.dateByAddingDays(vaccinDelay.vaccineDelayMax)
        let vaccineBoosterDelay = lastStateDate.dateByAddingDays(vaccinDelay.vaccineBoosterDelay)
        let vaccineDelayJanssen = lastStateDate.dateByAddingDays(vaccinDelay.vaccineDelayJanssen)
        let vaccineDelayMaxJanssen = lastStateDate.dateByAddingDays(vaccinDelay.vaccineDelayMaxJanssen)
        let vaccineBoosterDelayUnderAge = lastStateDate.dateByAddingDays(vaccinDelay.vaccineBoosterDelayUnderAge)
        let vaccineDelayMaxRecovery = lastStateDate.dateByAddingDays(vaccinDelay.vaccineDelayMaxRecovery)
        let vaccineBoosterDelayMax = lastStateDate.dateByAddingDays(vaccinDelay.vaccineBoosterDelayMax)
        
        let injectionCount = Int(certificate.completeCycleDosesCount?.value ?? "0") ?? 0
        
        if !validity {
            resultState = .nonValid
            globalValidity = .invalid
            // janssen
        } else if isFinished && (injectionCount == 1 && isJanssen) &&
                    (today >= vaccineDelayJanssen && today <= vaccineDelayMaxJanssen) {
            resultState = .valid
            globalValidity = .valid
            // other + age
        } else if isFinished {
            let birth = certificate.birthDateString
            
            guard let components = try? DateComponents(ISO8601String: vaccinDelay.vaccineBoosterAgeDuration).opposite(),
                  let vaccineBoosterAge = Calendar.current.date(byAdding: components, to: Date()),
                  let birthDate = birth?.formatBirthDayStringTo2DDocDate() else {
                      return nil
                  }
            
            let boosterDoseNeeded = birthDate < vaccineBoosterAge
            
            if !boosterDoseNeeded,
               (!isJanssen && injectionCount == 1 && today >= vaccineDelay) ||
                (injectionCount == 2 && today >= vaccineDelay) ||
                (injectionCount >= 3 && today >= vaccineBoosterDelayUnderAge) {
                resultState = .valid
                globalValidity = .valid
            } else if boosterDoseNeeded,
                      (!isJanssen && injectionCount == 1 && today >= vaccineDelay && today <= vaccineDelayMaxRecovery) ||
                        (injectionCount == 2 && today >= vaccineDelay && today <= vaccineDelayMax) ||
                        (injectionCount > 2 && today >= vaccineBoosterDelay && today <= vaccineBoosterDelayMax) {
                resultState = .valid
                globalValidity = .valid
            } else {
                resultState = .nonValid
                globalValidity = .invalid
            }
        } else {
            resultState = .nonValid
            globalValidity = .invalid
        }
        
        return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: ConstStat.TACV_2DDOC_IOS_LITE, ressourceType:  ConstStat.D_DOC_L1), certHash: certificate.getIdHash())
    }
}
