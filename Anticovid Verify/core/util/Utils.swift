//
//  Utils.swift
//  Anticovid Verify
//
//

import Foundation

class Utils {
    
    static func extractJson(from path: String) -> String? {
        if let json = try? String(contentsOfFile: path) {
            return json
        }
        return nil
    }
}
