//
//  UserData.swift
//  Anticovid Verify
//
//

import Foundation
import INCore

class UserDataManager {
    static let sharedInstance = UserDataManager()
    
    let vaccineDateKey = "vaccineDateKey"
    let vaccinePassSelectedKey = "vaccinePassSelectedKey"
    let shouldShowPrivacyKey = "shouldShowPrivacy"
    let shouldShowScanTutorialKey = "shouldShowScanTutorial"
    let shouldShowResultTutorialKey = "shouldShowResultTutorialVers2"
    let isNetworkConnectedKey = "isNetworkConnected"
    let hasMigrateToCriticalVersionKey = "hasMigrateTo176Key"
    let expirationDateMaskedControllerKey = "expirationDateMaskedController"
    let previousAppVersionForSyncKey = "previousAppversionForSyncKey"
    let appLastMajorVersionAcceptedKey = "appLastMajorVersionAcceptedKey"
    let appLastMinorVersionAcceptedKey = "appLastMinorVersionAcceptedKey"

    private init() {}

    var shouldShowPrivacy: Bool {
        get {
            return try! INPersistentData.getBool(forKey: shouldShowPrivacyKey, encrypted: false, defaultValue: true)
        }
        set(value) {
            try! INPersistentData.saveBool(key: shouldShowPrivacyKey, value: value, encrypted: false)
        }
    }
    
    var shouldShowScanTutorial: Bool {
        get {
            return try! INPersistentData.getBool(forKey: shouldShowScanTutorialKey, encrypted: false, defaultValue: true)
        }
        set(value) {
            try! INPersistentData.saveBool(key: shouldShowScanTutorialKey, value: value, encrypted: false)
        }
    }
    
    var shouldShowResultTutorial: Bool {
        get {
            return try! INPersistentData.getBool(forKey: shouldShowResultTutorialKey, encrypted: false, defaultValue: true)
        }
        set(value) {
            try! INPersistentData.saveBool(key: shouldShowResultTutorialKey, value: value, encrypted: false)
        }
    }

    var isNetworkConnected: Bool {
        get {
            return UserDefaults.standard.bool(forKey: isNetworkConnectedKey)
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: isNetworkConnectedKey)
            UserDefaults.standard.synchronize()
        }
    }
    var hasMigrateToCriticalVersion : Bool {
        get{
            return UserDefaults.standard.bool(forKey: hasMigrateToCriticalVersionKey)
        }
        set(value){
            UserDefaults.standard.set(value, forKey: hasMigrateToCriticalVersionKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var syncBannerViewMaskedDate: Date? {
        get {
            return UserDefaults.standard.value(forKey: expirationDateMaskedControllerKey) as? Date
        }
        set {
            UserDefaults.standard.set(Date(), forKey: expirationDateMaskedControllerKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var previousAppVersionForSync: String? {
        get {
            return UserDefaults.standard.string(forKey: previousAppVersionForSyncKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: previousAppVersionForSyncKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var appLastMajorVersionAccepted: Int? {
        get {
            return UserDefaults.standard.integer(forKey: appLastMajorVersionAcceptedKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: appLastMajorVersionAcceptedKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var appLastMinorVersionAccepted: Int? {
        get {
            return UserDefaults.standard.integer(forKey: appLastMinorVersionAcceptedKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: appLastMinorVersionAcceptedKey)
            UserDefaults.standard.synchronize()
        }
    }
}
