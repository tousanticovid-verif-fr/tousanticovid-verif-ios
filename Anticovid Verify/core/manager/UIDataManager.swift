//
//  UIDataManager.swift
//  Anticovid Verify
//
//

import Foundation
import INCore

class UIDataManager {

    static let shared = UIDataManager()
    
    let premiumManager: PremiumManager
    
    private let appLabelsKey = "appLabelsKey"
    private let tutorialOTEnKey = "uidata.tutorialOT.en"
    private let tutorialOTFrKey = "uidata.tutorialOT.fr"
    private let tutorialLiteEnKey = "uidata.tutorialLite.en"
    private let tutorialLiteFrKey = "uidata.tutorialLite.fr"
    private let blacklistLiteKey = "uidata.blacklistLite"
    private let blacklistOTKey = "uidata.blacklistOT"
    private let duplicateKey = "uidata.duplicate"
    private let messageOTModeFRKey = "uidata.messageOTMode"
    private let tutorialVaccinePassKey = "uidata.tutorialVaccinePass.fr"
    private let tutorialHealthPassKey = "uidata.tutorialHealthPass.fr"
    
    init(premiumManager: PremiumManager = .shared) {
        self.premiumManager = premiumManager
    }

    private var tutorialOTFr: String? {
        get{
            return try? INPersistentData.getString(forKey: tutorialOTFrKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: tutorialOTFrKey, value: value ?? "")
        }
    }
    
    private var tutorialOTEn: String? {
        get{
            return try? INPersistentData.getString(forKey: tutorialOTEnKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: tutorialOTEnKey, value: value ?? "")
        }
    }
    
    private var tutorialLiteFr: String? {
        get {
            return try? INPersistentData.getString(forKey: tutorialLiteFrKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: tutorialLiteFrKey, value: value ?? "")
        }
    }
    
    private var tutorialLiteEn: String? {
        get {
            return try? INPersistentData.getString(forKey: tutorialLiteEnKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: tutorialLiteEnKey, value: value ?? "")
        }
    }
    
    var tutorialVaccinePass: String? {
        get {
            return try? INPersistentData.getString(forKey: tutorialVaccinePassKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: tutorialVaccinePassKey, value: value ?? "")
        }
    }
    
    var tutorialHealthPass: String? {
        get {
            return try? INPersistentData.getString(forKey: tutorialHealthPassKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: tutorialHealthPassKey, value: value ?? "")
        }
    }

    private var blacklistLite: BlacklistMessageDetail? {
        get{
            if let blacklistLiteJson = UserDefaults.standard.object(forKey: blacklistLiteKey) as? Data {
                let decoder = JSONDecoder()
                return try? decoder.decode(BlacklistMessageDetail.self, from: blacklistLiteJson)
            }
            return nil
        }
        set(value){
            if let encoded = try? JSONEncoder().encode(value) {
                UserDefaults.standard.set(encoded, forKey: blacklistLiteKey)
            }
        }
    }
    
    private var blacklistOT: BlacklistMessageDetail? {
        get{
            if let blacklistLiteJson = UserDefaults.standard.object(forKey: blacklistOTKey) as? Data {
                let decoder = JSONDecoder()
                return try? decoder.decode(BlacklistMessageDetail.self, from: blacklistLiteJson)
            }
            return nil
        }
        set(value){
            if let encoded = try? JSONEncoder().encode(value) {
                UserDefaults.standard.set(encoded, forKey: blacklistOTKey)
            }
        }
    }
    
    private var duplicate: BlacklistMessageDetail? {
        get{
            if let blacklistLiteJson = UserDefaults.standard.object(forKey: duplicateKey) as? Data {
                let decoder = JSONDecoder()
                return try? decoder.decode(BlacklistMessageDetail.self, from: blacklistLiteJson)
            }
            return nil
        }
        set(value){
            if let encoded = try? JSONEncoder().encode(value) {
                UserDefaults.standard.set(encoded, forKey: duplicateKey)
            }
        }
    }
    
    var messageOtModeFR: String? {
        get{
            return try? INPersistentData.getString(forKey: messageOTModeFRKey, defaultValue: "")
        }
        set(value){
            try? INPersistentData.saveString(key: messageOTModeFRKey, value: value ?? "")
        }
    }
    
    func save(specificValues: SpecificValue) {
        self.tutorialLiteEn = specificValues.tutorial.liteEN
        self.tutorialLiteFr = specificValues.tutorial.liteFR

        self.tutorialOTEn = specificValues.tutorial.otEN
        self.tutorialOTFr = specificValues.tutorial.otFR
        
        self.tutorialHealthPass = specificValues.labels.health.tutorial.fr
        self.tutorialVaccinePass = specificValues.labels.vaccine.tutorial.fr

        self.blacklistLite = specificValues.blacklist.messages.blacklistLite
        self.blacklistOT = specificValues.blacklist.messages.blacklistOT
        self.duplicate = specificValues.blacklist.messages.blacklistOT
        
        self.messageOtModeFR = specificValues.ruleEngine?.messageOtModeFR
        
        self.uiLabels = specificValues.labels
    }

    func getLiteTutorial() -> String {
        var tutorialBase64 = ""
        if Locale.current.languageCode?.uppercased().contains("FR") ?? false {
            tutorialBase64 = tutorialLiteFr ?? ""
        } else {
            tutorialBase64 = tutorialLiteEn ?? ""
        }
        return tutorialBase64.base64ToString()
    }
    
    func getOtTutorial() -> String {
        var tutorialBase64 = ""
        if Locale.current.languageCode?.uppercased().contains("FR") ?? false {
            tutorialBase64 = tutorialOTFr ?? ""
        } else {
            tutorialBase64 = tutorialOTEn ?? ""
        }
        return tutorialBase64.base64ToString()
    }
    
    func getBlacklist() -> (String, String) {
        if Locale.current.languageCode?.uppercased().contains("FR") ?? false {
            if premiumManager.isPremium {
                return (blacklistOT?.titleFR ?? "", blacklistOT?.detailFR.base64ToString() ?? "")
            }
            return (blacklistLite?.titleFR ?? "", blacklistLite?.detailFR.base64ToString() ?? "")
        } else {
            if premiumManager.isPremium {
                return (blacklistOT?.titleEN ?? "", blacklistOT?.detailEN.base64ToString() ?? "")
            }
            return (blacklistLite?.titleEN ?? "", blacklistLite?.detailEN.base64ToString() ?? "")
        }
    }
    
    func getDuplicate() -> (String, String) {
        if Locale.current.languageCode?.uppercased().contains("FR") ?? false {
            return (duplicate?.titleFR ?? "", duplicate?.detailFR.base64ToString() ?? "")
        } else {
            return (duplicate?.titleEN ?? "", duplicate?.detailEN.base64ToString() ?? "")
        }
    }
    
    var uiLabels: Labels? {
        get {
            if let data = UserDefaults.standard.data(forKey: appLabelsKey),
               let labels = try? JSONDecoder().decode(Labels.self, from: data) {
                return labels
            }
            
            return nil
        }
        set {
            if let value = newValue,
                let encoded = try? JSONEncoder().encode(value) {
                UserDefaults.standard.set(encoded, forKey: appLabelsKey)
            }
        }
    }
}
    
