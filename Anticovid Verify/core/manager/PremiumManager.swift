//
//  PremiumManager.swift
//  Anticovid Verify
//
//

import Foundation
import INCore

class PremiumManager {
    
    static let shared = PremiumManager()
    
    private let expirationDateForOTKey = "expirationDateForOT"
    private let tokenOTKey = "tokenOT"
    private let sirenKey = "siren"
    private let modeSelectedKey = "modeSelectedKey"
    private let modeConfigurationsKey = "modeConfigurationsNewKey"
    private let shouldShowFullResultKey = "shouldShowFullResult"
    
    private(set) var expirationDateForOT: Double {
        get {
            return UserDefaults.standard.double(forKey: expirationDateForOTKey)
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: expirationDateForOTKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private(set) var tokenOT: String {
        get {
            return try! INPersistentData.getString(forKey: tokenOTKey, encrypted: false, defaultValue: "")
        }
        set(value) {
            modeSelected = !value.isEmpty ? .OT : modesAvailable().first!
            try! INPersistentData.saveString(key: tokenOTKey, value: value, encrypted: false)
        }
    }
    
    private(set) var siren: String {
        get {
            return try! INPersistentData.getString(forKey: sirenKey, encrypted: false, defaultValue: "")
        }
        set(value) {
            try! INPersistentData.saveString(key: sirenKey, value: value, encrypted: false)
        }
    }
    
    var modeConfigurations: [ModeSelected] {
        get {
            
            
            if let data = UserDefaults.standard.data(forKey: modeConfigurationsKey), let modeConfigs = try? JSONDecoder().decode([ModeSelected].self, from: data) {
                    return modeConfigs
                }
            return []
        }
        
        set(newValue) {
            if let encoded = try? JSONEncoder().encode(newValue) {
                UserDefaults.standard.set(encoded, forKey: modeConfigurationsKey)
                UserDefaults.standard.synchronize()
                if let _ = UserDefaults.standard.data(forKey: modeConfigurationsKey){
                }
            }
        }
    }
    
    var modeSelected: ModeSelected {
        get {
            if let data = UserDefaults.standard.data(forKey: modeSelectedKey),
            let modeSaved = try? JSONDecoder().decode(ModeSelected.self, from: data) {
                return modeSaved
            }
            
            return modesAvailable(allowGetOnModeSelected: false).first!
        }
        
        set(value) {
            if let encoded = try? JSONEncoder().encode(value) {
                UserDefaults.standard.set(encoded, forKey: modeSelectedKey)
            }
            
            var modesConfig = modeConfigurations
            if let indexForConfigs = modesConfig.firstIndex(of: value) {
                modesConfig.remove(at: indexForConfigs)
            }
            modesConfig.append(value)
            modeConfigurations = modesConfig
        }
    }
    
    private(set) var isPremium: Bool {
        get {
            return try! INPersistentData.getBool(forKey: shouldShowFullResultKey, encrypted: false, defaultValue: false)
        }
        set(value) {
            try! INPersistentData.saveBool(key: shouldShowFullResultKey, value: value, encrypted: false)
        }
    }
    
    /**
     Returns the # modes of the app.
     */
    func modesAvailable(allowGetOnModeSelected: Bool = true) -> [ModeSelected] {
        var vaccinePassActivated = false
        if let vaccinePassDate = RulesMotorRepository.shared.getMotorRules()?.vaccinePassStartDate {
            vaccinePassActivated = vaccinePassDate <= Date()
        }
        let defaultPass = vaccinePassActivated ? PassSelected.vaccine : PassSelected.health
        let defaultSelection: [ModeSelected] = isPremium ?
        [.tacVerif(passSelected: defaultPass, at: Date()), .tacVerifPlus, .OT] :
        [.tacVerif(passSelected: defaultPass, at: Date())]
               
        // added save configuration for possible modes
        let selection = defaultSelection.map { defaultMode -> ModeSelected in
            let savedConfigForMode = modeConfigurations

            var resultMode = defaultMode
            
            // specific actions for tac verif mode (check activation of vaccine pass)
            if let index = savedConfigForMode.firstIndex(of: defaultMode),
                case let .tacVerif(passSelected, passSelectedAtDate) = savedConfigForMode[index] {
                
                if passSelected == .vaccine,
                   let vaccinePassDate = RulesMotorRepository.shared.getMotorRules()?.vaccinePassStartDate,
                   vaccinePassDate > Date() { // vaccinePassDesactivated
                    resultMode = .tacVerif(passSelected: .health, at: Date())
                    UserDataManager.sharedInstance.shouldShowResultTutorial = true
                } else if passSelected == .health,
                          let vaccinePassDate = RulesMotorRepository.shared.getMotorRules()?.vaccinePassStartDate,
                          vaccinePassDate <= Date()  // vaccinePassActivated
                            && passSelectedAtDate < vaccinePassDate { // pass selected before activation of vaccine pass
                    resultMode = .tacVerif(passSelected: .vaccine, at: Date())
                    UserDataManager.sharedInstance.shouldShowResultTutorial = true
                } else if let index = savedConfigForMode.firstIndex(of: defaultMode) {
                    resultMode = savedConfigForMode[index].selectedAt(Date())
                }
            } else if let index = savedConfigForMode.firstIndex(of: defaultMode) {
                resultMode = savedConfigForMode[index]
            }
            
            // replaces values
            if allowGetOnModeSelected, modeSelected == defaultMode {
                self.modeSelected = resultMode
            } else if let index = savedConfigForMode.firstIndex(of: resultMode) {
                modeConfigurations.remove(at: index) // modify user configs
                modeConfigurations.append(resultMode)
            }
                        
            return resultMode
        }
        
        return selection
    }
    
    
    
    func resetPremium() {
        isPremium = false
        tokenOT = ""
        siren = ""
        modeSelected = modesAvailable(allowGetOnModeSelected: false).first!
        expirationDateForOT = -1
        modeConfigurations.removeAll(where: {
            $0 == .OT || $0 == .tacVerifPlus
        })
    }
    
    func scheduleResetPremiumIfNeeded() {
        let timeinterval = TimeInterval(PremiumManager.shared.expirationDateForOT)
        let expirationDate = Date(timeIntervalSince1970: timeinterval)
        if Date() > expirationDate {
            PremiumManager.shared.resetPremium()
            NotificationCenter.default.post(name: ConstNotification.navControllerAppareance, object: nil)
        } else {
            let expirationOTToken = timeinterval - Date().timeIntervalSince1970
            
            DispatchQueue.main.asyncAfter(deadline: .now().advanced(by: .seconds(Int(expirationOTToken)))) {
                PremiumManager.shared.resetPremium()
                NotificationCenter.default.post(name: ConstNotification.navControllerAppareance, object: nil)
            }
        }
    }
}

enum ModeSelected: Codable, Equatable {
    static var tacVerifAny: ModeSelected {
        get {
            .tacVerif(passSelected: .vaccine, at: Date())
        }
    }
    
    case tacVerif(passSelected: PassSelected, at: Date)
    case tacVerifPlus
    case OT
    
    func selectedAt(_ date: Date) -> Self {
        switch self {
        case .tacVerif(let passSelected, _):
            return .tacVerif(passSelected: passSelected, at: date)
        case .tacVerifPlus, .OT:
            return self
        }
    }
    
    static func ==(lhs: Self, rhs: Self) -> Bool {
        switch lhs {
        case .tacVerif:
            switch rhs {
            case .tacVerif:
                return true
            case .tacVerifPlus:
                return false
            case .OT:
                return false
            }
        case .tacVerifPlus:
            switch rhs {
            case .tacVerif:
                return false
            case .tacVerifPlus:
                return true
            case .OT:
                return false
            }
        case .OT:
            switch rhs {
            case .tacVerif:
                return false
            case .tacVerifPlus:
                return false
            case .OT:
                return true
            }
        }
    }
}

enum PassSelected: String, Codable {
    case vaccine
    case health
}


