//
//  ConfDataManager.swift
//  Anticovid Verify
//
//

import Foundation
import INCore

class ConfDataManager {
    
    public static let shared = ConfDataManager()
    
    let duplicateGestureActivationKey = "duplicate.activation"
    let duplicateBackupTimeKey = "duplicate.backupTime"
    let isExpirationDateCheckedForVaccineKey = "expirationDate.enabled.vaccine"
    let isExpirationDateCheckedForTestKey = "expirationDate.enabled.test"
    let isExpirationDateCheckedForRecoveryKey = "expirationDate.enabled.recovery"
    let isExpirationDateCheckedForExemptionKey = "expirationDate.vaccine.exemption"

    var duplicateGestureActivation: Bool {
        get { value(forKey: duplicateGestureActivationKey) }
        set(value) { save(value, forKey: duplicateGestureActivationKey) }
    }
    
    var duplicateBackupTime: Int {
        get { value(forKey: duplicateBackupTimeKey) }
        set(value) { save(value, forKey: duplicateBackupTimeKey) }
    }
    
    var isExpirationDateCheckedForVaccine: Bool {
        get { value(forKey: isExpirationDateCheckedForVaccineKey) }
        set(value) { save(value, forKey: isExpirationDateCheckedForVaccineKey) }
    }
    
    var isExpirationDateCheckedForTest: Bool {
        get { value(forKey: isExpirationDateCheckedForTestKey) }
        set(value) { save(value, forKey: isExpirationDateCheckedForTestKey) }
    }

    var isExpirationDateCheckedForRecovery: Bool {
        get { value(forKey: isExpirationDateCheckedForRecoveryKey) }
        set(value) { save(value, forKey: isExpirationDateCheckedForRecoveryKey) }
    }

    var isExpirationDateCheckedForExemption: Bool {
        get { value(forKey: isExpirationDateCheckedForExemptionKey) }
        set(value) { save(value, forKey: isExpirationDateCheckedForExemptionKey) }
    }
    var remindHoursStep1 : Double{
        get{
            return UserDefaults.standard.object(forKey: CertificatesManager.certificatesRemindStep1Key) as? Double ?? 0
        }
        set(value){
            UserDefaults.standard.set(value,forKey: CertificatesManager.certificatesRemindStep1Key)
        }
    }
    
    var remindHoursStep2 : Double{
        get{
            return UserDefaults.standard.object(forKey: CertificatesManager.certificatesRemindStep2Key) as? Double ?? 0
        }
        set(value){
            UserDefaults.standard.set(value,forKey: CertificatesManager.certificatesRemindStep2Key)
        }
    }
    
    var remindHoursStep3 : Double{
        get{
            return UserDefaults.standard.object(forKey: CertificatesManager.certificatesRemindStep3Key) as? Double ?? 0
        }
        set(value){
            UserDefaults.standard.set(value,forKey: CertificatesManager.certificatesRemindStep3Key)
        }
    }
    
    
    var remindFrequency : Double{
        get{
            return UserDefaults.standard.object(forKey: CertificatesManager.certificatesFrequencyKey) as? Double ?? 0
        }
        set(value){
            UserDefaults.standard.set(value,forKey: CertificatesManager.certificatesFrequencyKey)
        }
    }

    func save(specificValues: SpecificValue) {
        duplicateGestureActivation = specificValues.blacklist.duplicateActivate
        duplicateBackupTime = specificValues.blacklist.duplicateRetentionPeriod
        isExpirationDateCheckedForVaccine = specificValues.useExpirationDate.forVaccine
        isExpirationDateCheckedForTest = specificValues.useExpirationDate.forTest
        isExpirationDateCheckedForRecovery = specificValues.useExpirationDate.forRecovery
        isExpirationDateCheckedForExemption = specificValues.useExpirationDate.forExemption
        
    }
    func save(remindData: CertificatesUpdateResponse) {
        guard let remindStep1 = remindData.period?.step1,
              let remindStep2 = remindData.period?.step2,
              let remindStep3 = remindData.period?.step3,
              let frequency = remindData.period?.frequency else {return}
        remindHoursStep1 = remindStep1.toDouble()
        remindHoursStep2 = remindStep2.toDouble()
        remindHoursStep3 = remindStep3.toDouble()
        remindFrequency = frequency.toDouble()
    }
}

private extension ConfDataManager {
    func value<T>(forKey key: String) -> T {
        switch T.self {
        case is Int.Type:
            return try! INPersistentData.getInt(forKey: key,
                                                    encrypted: false,
                                                    defaultValue: 1) as! T
        case is Bool.Type:
            return try! INPersistentData.getBool(forKey: key,
                                                     encrypted: false,
                                                     defaultValue: false) as! T
        default:
            fatalError("⚠️ No behavior defined for this type")
        }
    }
    
    func save<T>(_ value: T, forKey key: String) {
        switch T.self {
        case is Int.Type:
            try! INPersistentData.saveInt(key: key,
                                              value: value as! Int,
                                              encrypted: false)
        case is Bool.Type:
            try! INPersistentData.saveBool(key: key,
                                               value: value as! Bool,
                                               encrypted: false)
        default:
            fatalError("⚠️ No behavior defined for this type")
        }
    }
}
