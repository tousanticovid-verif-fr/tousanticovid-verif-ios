//
//  MotorRulesManager.swift
//  Anticovid Verify
//
//

import Foundation
import INCore
import CertLogic
import SwiftDGC
import OSLog

struct MotorRulesConf {
    let filter: FilterParameter
    let external: ExternalParameter
}

class CertLogicMotorManager {
    
    static let shared = CertLogicMotorManager()
    
    static let schemaKey = "motorRules.schema"
    
    let certLogicMotorRepository: CertLogicMotorRepository
    
    let certLogicEngine: CertLogicEngine
    
    init(certLogicMotorRepository: CertLogicMotorRepository = .shared) {
        self.certLogicMotorRepository = certLogicMotorRepository
        self.certLogicEngine = CertLogicEngine(schema: SwiftDGC.euDgcSchemaV1, rules: certLogicMotorRepository.getRules())
    }
    
    func canUseCertLogicEngine() -> Bool {
        return !certLogicMotorRepository.getRules().isEmpty && !certLogicMotorRepository.getValueSets().isEmpty
    }
    
    func handleRuleBasesResponse(_ ruleBases: [RuleBaseResponse]) throws -> RulesUpdateState {
        var savedRules = certLogicMotorRepository.getRules()
        
        // add rules not saved yet
        let ruleBasesToSave = ruleBases.filter { responseRule in
            !savedRules.contains(where: { $0.hash == responseRule.hash })
        }
        FileLogger.shared.write(text: "add \(ruleBasesToSave.count) rulesId")

        try certLogicMotorRepository.save(ruleBases: ruleBasesToSave)
        
        // remove deleted rules
        savedRules = certLogicMotorRepository.savedRules()
        
        let savedRuleToRemove = savedRules.filter { savedRule in
            !ruleBases.contains(where: { $0.hash == savedRule.hash })
        }
        
        try certLogicMotorRepository.delete(ruleHashes: savedRuleToRemove.compactMap { $0.hash })
        
        return .init(rulesToAdd: ruleBasesToSave)
    }
    
    func handleValueSetBasesResponse(_ valueSets: [ValueSetBaseResponse]) throws -> ValueSetsUpdateState {
        var valueSetsStored = certLogicMotorRepository.getValueSets()
        
        // add ValueSets not saved yet
        let toSave = valueSets.filter { responseSet in
            !valueSetsStored.contains(where: { $0.hash == responseSet.hash })
        }
        
        FileLogger.shared.write(text: "add \(toSave.count) valueSetId")
        try certLogicMotorRepository.save(valueSetBases: toSave)
    
        valueSetsStored = certLogicMotorRepository.savedValueSets()

        // remove deleted ValueSets
        let savedSetsIdsToRemove = valueSetsStored.filter { saveSet in
            !valueSets.contains(where: { $0.hash == saveSet.hash })
        }
        
        try certLogicMotorRepository.delete(valueSetHashes: savedSetsIdsToRemove.compactMap { $0.hash })
        
        return .init(valueSetToAdd: toSave)
    }

    func handleValueSetsResponse(_ valueSets: [CertLogic.ValueSet], withIds valueSetBases: [ValueSetBaseResponse]) throws {
        FileLogger.shared.write(text: "add \(valueSets.count) ValueSet")
        
        let valueSetsEdited = valueSets.map { valueSet -> ValueSet in
            let firstBase = valueSetBases.first(where: { valueSet.valueSetId == $0.id })
            valueSet.hash = firstBase?.hash
            
            return valueSet
        }
        
        try certLogicMotorRepository.save(valueSets: valueSetsEdited)
    }
    
    func handleRulesResponse(_ rules: [CertLogic.Rule], withIds ruleBases: [RuleBaseResponse]) throws {
        FileLogger.shared.write(text: "add \(rules.count) rules")
        
        let rulesEdited = rules.map { rule -> Rule in
            let firstBase = ruleBases.first(where: { rule.identifier == $0.identifier && rule.version == $0.version })
            rule.hash = firstBase?.hash
            
            return rule
        }
        
        try certLogicMotorRepository.save(rules: rulesEdited)
    }
    
    /**
     Checks for special rules that do not need validation from cert logic engine
     */
    func handleCertLogicPreValidation(with rules: [Rule]) -> [ValidationResult]? {
        guard rules.contains(modifier: .exclusion) else {
            return nil
        }
        
        return [ValidationResult(rule: nil, result: .open, validationErrors: nil)]
    }
    
    /**
     Validation for single country
     */
    func validate(certificate: HCert, country: String, date: Date, region: String? = nil, with rules: [Rule]) -> [ValidationResult] {
        
        if let preValidation = handleCertLogicPreValidation(with: rules) {
            return preValidation
        }
        
        let valueSet: Dictionary<String, [String]> = certLogicMotorRepository.getValueSets().toDictionnaryForExternalParameters()
       
        certLogicEngine.updateRules(rules: rules)

        let filter = FilterParameter(validationClock: date, countryCode: country.uppercased(), certificationType: getCertificationType(type: certificate.type), region: region)
        let external = ExternalParameter(validationClock: date, valueSets: valueSet, exp: certificate.exp, iat: certificate.iat, issuerCountryCode: certificate.issCode)
        
        let date = Date()
        os_log("%@",type: .info, "🚗 Will validate with certlogic at: \(date.dateTimeString)")
        var res = certLogicEngine.validate(filter: filter, external: external, payload: certificate.body.description)
        res = Array(Set(res))
        
        let date2 = Date()
        os_log("%@",type: .info, "🚗 Did finish validating with certlogic at: \(date2.dateTimeString) ...took \(date.distance(to: date2)) seconds")

        return res
    }
    
    func getRuleDetailsError(rule: Rule, filter: FilterParameter) -> Dictionary<String, String> {
        return certLogicEngine.getDetailsOfError(rule: rule, filter: filter)
    }
    
    func loadLocalRulesId() -> [RuleBaseResponse]?{
        if let jsonPath = Bundle.main.url(forResource: "sync_rules_id", withExtension: "json") {
            do{
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                if let response = try? decoder.decode([RuleBaseResponse].self, from: contents){
                    return response
                }
            } catch {
                return nil
            }
        }
        return nil
    }
    func loadLocalRules() -> [CertLogic.Rule]?{
        if let jsonPath = Bundle.main.url(forResource: "sync_rules", withExtension: "json") {
            do{
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                if let response = try? decoder.decode([CertLogic.Rule].self, from: contents){
                    return response
                }
            } catch {
                return nil
            }
        }
        return nil
    }
    func loadLocalValueSetId() -> [ValueSetBaseResponse]?{
        if let jsonPath = Bundle.main.url(forResource: "sync_valueset_id", withExtension: "json") {
            do{
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                if let response = try? decoder.decode([ValueSetBaseResponse].self, from: contents){
                    return response
                }
            } catch {
                return nil
            }
        }
        return nil
    }
    func loadLocalValueSet() -> [CertLogic.ValueSet]?{
        if let jsonPath = Bundle.main.url(forResource: "sync_valueset", withExtension: "json") {
            do{
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                if let response = try? decoder.decode([CertLogic.ValueSet].self, from: contents){
                    return response
                }
            } catch {
                return nil
            }
        }
        return nil
    }
    
    func loadLocalBL2DDoc() -> BlacklistResponse?{
        if let jsonPath = Bundle.main.url(forResource: "sync_bl_2ddoc", withExtension: "json") {
            do{
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                if let response = try? decoder.decode(BlacklistResponse.self, from: contents){
                    return response
                }
            } catch {
                return nil
            }
        }
        return nil
    }
    func loadLocalBLDCC() -> BlacklistResponse?{
        if let jsonPath = Bundle.main.url(forResource: "sync_bl_dcc", withExtension: "json") {
            do{
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                if let response = try? decoder.decode(BlacklistResponse.self, from: contents){
                    return response
                }
            } catch {
                return nil
            }
        }
        return nil
    }
}

extension CertLogicMotorManager {
    
    func getCertificationType(type: SwiftDGC.HCertType) -> CertificateType {
        var certType: CertificateType = .general
        switch type {
        case .recovery:
            certType = .recovery
        case .test:
            certType = .test
        case .vaccine:
            certType = .vaccination
        case .unknown:
            certType = .general
        case .exampted:
            certType = .general
        case .activity:
            certType = .general
        }
        return certType
    }
}
