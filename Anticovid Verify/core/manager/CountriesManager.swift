//
//  CountryManager.swift
//  Anticovid Verify
//

import Foundation

class CountriesManager : NSObject {
    private let countryRepository: CountryRepository
    private let certLogicRepository: CertLogicMotorRepository

    static let shared = CountriesManager()
    
    private(set) lazy var countries: [Country] = {
        try? loadLocalCountries()
        return countries
    }()
    
    init(countryRepository: CountryRepository = .shared, certLogicRepository: CertLogicMotorRepository = .shared) {
        self.countryRepository = countryRepository
        self.certLogicRepository = certLogicRepository
    }
    
    func loadLocalCountries() throws {
        let result = try countryRepository.getAll()
        
        if result.isEmpty {
            try loadLocalCountriesFromResourcesAndRules()
        } else {
            countries = result.sorted { $0.name < $1.name }
        }
    }
    
    /**
     Load local countries (from resources and local database)
     */
    func loadLocalCountriesFromResourcesAndRules() throws {
        let countriesFromLocalResources = loadCountries(withFile: Bundle.main.url(forResource: "Country", withExtension: "json"))
        
        var result = Array(Set(countriesFromLocalResources))
        
        let ruleDeparturesCountries = try certLogicRepository.departureCountriesFromSavedRules()
            .compactMap { code -> Country? in
                if let countryName = NSLocale.current.localizedString(forRegionCode: code),
                   countryName != code {
                    return Country(name: countryName, nameCode: code.uppercased(), isNational: false, isDeparture: true)
                }
                
                return nil
            }
        
        let ruleDestinationCountries = try certLogicRepository.destinationCountriesFromSavedRules()
            .compactMap { code -> Country? in
                if let countryName = NSLocale.current.localizedString(forRegionCode: code),
                   countryName != code {
                    return Country(name: countryName, nameCode: code.uppercased(), isNational: false, isArrival: true)
                }
                
                return nil
            }
        
        ruleDeparturesCountries.forEach {
            if let idToReplace = result.firstIndex(of: $0) {
                result[idToReplace].isDeparture = true
            } else {
                result.append($0)
            }
        }
        
        ruleDestinationCountries.forEach {
            if let idToReplace = result.firstIndex(of: $0) {
                result[idToReplace].isArrival = true
            } else {
                result.append($0)
            }
        }
        
        countries = result.sorted { $0.name < $1.name }
        
        try storeCountries(countries)
    }
    
    func loadCountriesCodes(withFile fileURL: URL?) -> [String] {
        do {
            if let url = fileURL,
               let data = try Data(contentsOf: url) as Data?,
               let countries = try JSONDecoder().decode([String].self, from: data) as [String]? {
                return countries
            }
        } catch{
            NSLog(error.localizedDescription)
            return [String]()
        }
        return [String]()
    }
}

private extension CountriesManager {
    func storeCountries(_ countries: [Country]) throws  {
        try countryRepository.deleteAll()
        try countryRepository.save(countries: countries)
    }
    
    func loadCountries(withFile fileURL: URL?) -> [Country] {
        do {
            if let url = fileURL,
               let data = try Data(contentsOf: url) as Data?,
               let countries = try JSONDecoder().decode([Country].self, from: data) as [Country]? {
                return countries
            }
        } catch{
            NSLog(error.localizedDescription)
            return [Country]()
        }
        return [Country]()
    }
}
