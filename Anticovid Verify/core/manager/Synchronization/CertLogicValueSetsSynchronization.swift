//
//  CertLogicValueSetsSynchronization.swift
//  Anticovid Verify
//

import Foundation
import INCommunication
import INCore
import os.log
import Combine
import CertLogic

class CertLogicValueSetsSynchronization {
    private let certLogicMotorManager: CertLogicMotorManager
    private let countriesManager: CountriesManager
    
    init(certLogicMotorManager: CertLogicMotorManager = .shared,
         countriesManager: CountriesManager = .shared) {
        self.certLogicMotorManager = certLogicMotorManager
        self.countriesManager = countriesManager
    }
    
    func synchronizeValueSets() -> AnyPublisher<Void, Error> {
        fetchValueSetBases().flatMap(fetchValueSets(valueSetsIds:)).eraseToAnyPublisher()
    }
}

private extension CertLogicValueSetsSynchronization {
    func fetchValueSetBases() -> AnyPublisher<[ValueSetBaseResponse], Error> {
        let service = ServiceRouter.valueSetIds
        
        return INRequestService.shared.executeRequest(service,
                                                          type: [ValueSetBaseResponse].self,
                                                          timeout: 10,
                                                          pinCertificates: service.getPinningCerts())
            .tryMap { [unowned self] response in
                try self.certLogicMotorManager.handleValueSetBasesResponse(response).valueSetToAdd
            }
            .eraseToAnyPublisher()
    }
    
    func fetchValueSets(valueSetsIds: [ValueSetBaseResponse]) -> AnyPublisher<Void, Error> {
        guard !valueSetsIds.isEmpty else {
            return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        
        let service = ServiceRouter.valueSetDetails(valueSetIds: valueSetsIds.map { $0.hash } )
        
        return INRequestService.shared.executeRequest(service, type: [CertLogic.ValueSet].self,
                                                          timeout: 10,
                                                          pinCertificates: service.getPinningCerts())
            .tryMap { [unowned self, valueSetsIds] response in
                try self.certLogicMotorManager.handleValueSetsResponse(response, withIds: valueSetsIds)
            }
            .eraseToAnyPublisher()
        
    }
}


