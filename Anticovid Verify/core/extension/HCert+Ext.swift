//
//  HCert+Ext.swift
//  Anticovid Verify
//
//

import Foundation
import SwiftDGC
import OrderedCollections

extension HCert: BarcodeProtocolObject {
    
    func getIdHash(forCertField field: String) -> String {
        switch self.type {
        case .activity, .exampted:
            return "\(self.body[field]["co"].stringValue)\(self.body[field]["ci"].stringValue)".sha256()
        default:
            return "\(self.body[field][0]["co"].stringValue)\(self.body[field][0]["ci"].stringValue)".sha256()
        }
    }

    func getUIInformation(result: CertificateState, shouldShowFullResult: Bool) -> OrderedDictionary<String, [FieldRow]> {
        var informationsData: [FieldRow] = []
        var validityData: [FieldRow] = []
        
        let surname = self.body["nam"]["gn"].stringValue.isEmpty ? self.body["nam"]["gnt"].stringValue.uppercased() : self.body["nam"]["gn"].stringValue.uppercased()
        let name = self.body["nam"]["fn"].stringValue.isEmpty ? self.body["nam"]["fnt"].stringValue.uppercased() : self.body["nam"]["fn"].stringValue.uppercased()
        
        if !surname.isEmpty {
            informationsData.append(FieldRow.Field(title: "LISTE DES PRENOMS", subtitle: surname, picto: .eye))
        }
        if !name.isEmpty {
            informationsData.append(FieldRow.Field(title: "NOM DE FAMILLE", subtitle: name, picto: .eye))
        }
        
        if let dateOfBirthString = self.body["dob"].string {
            if dateOfBirthString.count == 4 {
                informationsData.append(FieldRow.Field(title: "DATE DE NAISSANCE", subtitle: "XX/XX/\(dateOfBirthString)", picto: .eye))
            } else if let dateOfBirth = Date(dateString: dateOfBirthString) {
                informationsData.append(FieldRow.Field(title: "DATE DE NAISSANCE", subtitle: dateOfBirth.dateString, picto: .eye))
            } else {
                informationsData.append(FieldRow.Field(title: "DATE DE NAISSANCE", subtitle: dateOfBirthString, picto: .eye))
            }
        } else {
            informationsData.append(FieldRow.Field(title: "DATE DE NAISSANCE", subtitle: self.body["dob"].stringValue, picto: .eye))
        }
        switch self.type {
        case .vaccine:
            validityData =
                [FieldRow.Field(title: "DURÉE DEPUIS LA DERNIÈRE INJECTION", subtitle: Date(dateString: self.body["v"][0]["dt"].string ?? "")?.formatForVaccinOT() ?? "?", picto: ResultPicto.none)]
            if !shouldShowFullResult { break }
            informationsData.append(FieldRow.Field(title: "DATE DE LA VACCINATION", subtitle: Date(dateString: self.body["v"][0]["dt"].string ?? "")?.dateString ?? "", picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "NOMBRE DANS UNE SÉRIE DE VACCIN/DOSES", subtitle: "\(self.body["v"][0]["dn"].intValue)/\(self.body["v"][0]["sd"].intValue)", picto: ResultPicto.none))
        
            informationsData.append(FieldRow.Field(title: "MÉDICAMENT VACCINAL", subtitle: Formatter.shared.vaccineName(from: self.body["v"][0]["mp"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.shared.diseaseName(from: self.body["v"][0]["tg"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "AGENT PROPHYLACTIQUE", subtitle: Formatter.shared.prophylacticAgentName(from: self.body["v"][0]["vp"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "FABRICANT", subtitle: Formatter.shared.manufacturerName(from: self.body["v"][0]["ma"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.shared.countryName(from: self.body["v"][0]["co"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "ÉMETTEUR DU CERTIFICAT", subtitle: self.body["v"][0]["is"].stringValue, picto: ResultPicto.none))

            informationsData.append(FieldRow.Field(title: "IDENTIFIANT DU CERTIFICAT", subtitle: self.body["v"][0]["ci"].stringValue, picto: ResultPicto.none))
        case .test:
            validityData = [FieldRow.Field(title: "DURÉE DEPUIS LE PRÉLÈVEMENT", subtitle: Date(dateString: self.body["t"][0]["sc"].stringValue)?.formatForTestOT() ?? "?", picto: ResultPicto.none)]
            if !shouldShowFullResult { break }
            informationsData.append(FieldRow.Field(title: "DATE ET HEURE DU PRELEVEMENT", subtitle: Date(dateString: self.body["t"][0]["sc"].stringValue)?.fullDateFormatted() ?? "", picto: ResultPicto.none))
            informationsData.append(FieldRow.Field(title: "RESULTAT DU TEST", subtitle: Formatter.shared.analysisResultFormatter(analysisResult: self.body["t"][0]["tr"].string), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "TYPE DE TEST", subtitle: Formatter.shared.testTypeName(from: self.body["t"][0]["tt"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.shared.diseaseName(from: self.body["t"][0]["tg"].stringValue), picto: ResultPicto.none))
            
            if let testName = self.body["t"][0]["nm"].string {
                informationsData.append(FieldRow.Field(title: "NOM DU TEST", subtitle: testName, picto: ResultPicto.none))
            }
            
            if let testManufacturer = self.body["t"][0]["ma"].string {
                informationsData.append(FieldRow.Field(title: "FABRICANT DU TEST", subtitle: Formatter.shared.manufacturerName(from: testManufacturer), picto: ResultPicto.none))
            }
            
            informationsData.append(FieldRow.Field(title: "CENTRE DE TEST", subtitle: self.body["t"][0]["tc"].stringValue, picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.shared.countryName(from: self.body["t"][0]["co"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "ÉMETTEUR DU CERTIFICAT", subtitle: self.body["t"][0]["is"].stringValue, picto: ResultPicto.none))

            informationsData.append(FieldRow.Field(title: "IDENTIFIANT DU CERTIFICAT", subtitle: self.body["t"][0]["ci"].stringValue, picto: ResultPicto.none))
        case .recovery:
            validityData = [FieldRow.Field(title: "DURÉE DEPUIS LE PRÉLÈVEMENT POSITIF", subtitle:
                                    Date(dateString: self.body["r"][0]["fr"].string ?? "")?.formatForRecoveryOT() ?? "?", picto: ResultPicto.none)]
            if !shouldShowFullResult { break }
            informationsData.append(FieldRow.Field(title: "DATE DU PREMIER PRÉLÈVEMENT POSITIF", subtitle: Date(dateString: self.body["r"][0]["fr"].string ?? "")?.dateString ?? "", picto:ResultPicto.none))
            
            if let startOfValidityString = self.body["r"][0]["df"].string, let startOfValidity = Date(dateString: startOfValidityString) {
                informationsData.append(FieldRow.Field(title: "DÉBUT DE VALIDITÉ", subtitle: startOfValidity.dateString, picto: ResultPicto.none))
            } else {
                informationsData.append(FieldRow.Field(title: "DÉBUT DE VALIDITÉ", subtitle: self.body["r"][0]["df"].stringValue, picto: ResultPicto.none))
            }
            
            if let endOfValidityString = self.body["r"][0]["du"].string, let endOfValidity = Date(dateString: endOfValidityString) {
                informationsData.append(FieldRow.Field(title: "FIN DE VALIDITÉ", subtitle: endOfValidity.dateString, picto: ResultPicto.none))
            } else {
                informationsData.append(FieldRow.Field(title: "FIN DE VALIDITÉ", subtitle: self.body["r"][0]["du"].stringValue, picto: ResultPicto.none))
            }
            
            informationsData.append(FieldRow.Field(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.shared.diseaseName(from: self.body["r"][0]["tg"].stringValue), picto: ResultPicto.none))
            
            informationsData.append(FieldRow.Field(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.shared.countryName(from: self.body["r"][0]["co"].stringValue), picto:ResultPicto.none))
            informationsData.append(FieldRow.Field(title: "ÉMETTEUR DU CERTIFICAT", subtitle: self.body["r"][0]["is"].stringValue, picto:ResultPicto.none))

            informationsData.append(FieldRow.Field(title: "IDENTIFIANT DU CERTIFICAT", subtitle: self.body["r"][0]["ci"].stringValue, picto:ResultPicto.none))
        case .exampted:
            validityData = [
                FieldRow.Field(title: "DATE DE DÉBUT DE VALIDITÉ", subtitle: Date(dateString: self.body["ex"]["df"].string ?? "")?.dateString ?? "", picto:ResultPicto.none),
                FieldRow.Field(title: "DATE DE FIN DE VALIDITÉ", subtitle: Date(dateString: self.body["ex"]["du"].string ?? "")?.dateString ?? "", picto:ResultPicto.none)
            ]
            if !shouldShowFullResult { break }
            
            informationsData.append(FieldRow.Field(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.shared.diseaseName(from: self.body["ex"]["tg"].stringValue), picto:ResultPicto.none))
    
            informationsData.append(FieldRow.Field(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.shared.countryName(from: self.body["ex"]["co"].stringValue), picto:ResultPicto.none))
            informationsData.append(FieldRow.Field(title: "ÉMETTEUR DU CERTIFICAT", subtitle: self.body["ex"]["is"].stringValue, picto:ResultPicto.none))
            informationsData.append(FieldRow.Field(title: "IDENTIFIANT DU CERTIFICAT", subtitle: self.body["ex"]["ci"].stringValue, picto:ResultPicto.none))
        case .activity:
            if !shouldShowFullResult { break }
            validityData.append(FieldRow.Field(title: "DATE ÉMISSION DU DOCUMENT", subtitle:iat.fullDateFormatted() , picto:ResultPicto.none))
            validityData.append(FieldRow.Field(title: "DATE EXPIRATION DU DOCUMENT", subtitle:exp.fullDateFormatted() , picto:ResultPicto.none))
        case .unknown:
            break
        }
        return shouldShowFullResult ? ["Données de validité": validityData, "Informations du QR Code": informationsData] : ["Informations du QR Code": informationsData]
    }
}
