//
//  GoOutData.swift
//  Anticovid Verify
//
//

import Foundation

struct GoOutData: Codable {
    
    var departureCountry: Country
    var favoritesCountries: [Country]
    var currentCheckDate: DateState
    
    init(departureCountry: Country, favoritesCountries: [Country] , currentCheckDate: DateState) {
        self.departureCountry = departureCountry
        self.favoritesCountries = favoritesCountries
        self.currentCheckDate = currentCheckDate
    }
}
