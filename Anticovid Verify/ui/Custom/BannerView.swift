//
//  BannerView.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

enum BannerType: String {
    case updateBannerSyncApp = "Synchronisation nécessaire"
    case updateApplicationRequired = "Nouvelle version requise. Procedez à la mise à jour de l'application"
    case updateApplicationAvailable = "Nouvelle version disponible"
    
    var priority: Int {
        // lower is most prioritized
        var result: Int
        switch self {
        case .updateApplicationRequired:
            result = 0
        case .updateApplicationAvailable:
            result = 1
        case .updateBannerSyncApp:
            result = 2
        }
        
        return result
    }
}

extension BannerType: Comparable {
    static func < (lhs: BannerType, rhs: BannerType) -> Bool {
        lhs.priority < rhs.priority
    }
    
    
}
