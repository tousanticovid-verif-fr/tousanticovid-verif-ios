//
//  INViewPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import INCommunication
import INCore

protocol INViewProtocol: AnyObject {
    func displaySynchronizationAlertIfNeeded(forRemind remindStep: RemindStep, didTapOnBanner: Bool)
}

class INViewPresenter {
    
    private let notificationCenter: NotificationCenter
        
    private let syncManager: SyncManager
    
    private weak var view: INViewProtocol!
    
    init(_ view: INViewProtocol, syncManager: SyncManager = .shared, notificationCenter: NotificationCenter = NotificationCenter.default) {
        self.notificationCenter = notificationCenter
        self.view = view
        self.syncManager = syncManager
    }

    func checkNeedAppSync() {
        let reminStep = syncManager.actualAppSyncRemindStep()
        switch reminStep {
        case .low, .medium, .high:
            self.notificationCenter.post(name: ConstNotification.updateBannerSyncApp, object: nil)
        default:
            self.view.displaySynchronizationAlertIfNeeded(forRemind: .none, didTapOnBanner: false)
        }
    }
}
