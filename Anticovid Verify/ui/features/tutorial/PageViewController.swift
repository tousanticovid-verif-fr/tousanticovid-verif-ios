//
//  PageViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class PageViewController : UIViewController {
    
    @IBOutlet weak var uiPageViewContainer: UIView!
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    var viewControllers = [UIViewController]()
    
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configurePageViewController()
        self.configurePageIndicator()
    }
    
    private func configurePageViewController() {
        let uiPageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        uiPageViewController.delegate = self
        uiPageViewController.dataSource = self
        uiPageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.replaceChildView(with: uiPageViewController)
        
        uiPageViewController.setViewControllers([viewControllers.first ?? UIViewController()], direction: .forward, animated: true, completion: nil)
    }
    
    private func configurePageIndicator() {
        self.pageIndicator.numberOfPages = viewControllers.count
        self.pageIndicator.isUserInteractionEnabled = false
        self.setPageIndicatorCurrentIndex()
    }
    
    private func replaceChildView(with uiViewController: UIViewController) {
        let currentChild = self.children.first
        
        currentChild?.willMove(toParent: nil)
        uiPageViewContainer.frame = uiViewController.view.frame
        uiViewController.view.alpha = 0
        
        self.addChild(uiViewController)
        self.view.isUserInteractionEnabled = false
        
        self.transition(from: currentChild!, to: uiViewController, duration: 0.1, options: [.curveEaseInOut], animations: {
            uiViewController.view.alpha = 1
            currentChild?.view.alpha = 0
        }, completion: { _ in
            currentChild?.removeFromParent()
            uiViewController.didMove(toParent: self)
            self.view.isUserInteractionEnabled = true
            self.uiPageViewContainer.isHidden = false
        })
    }
    
    private func setPageIndicatorCurrentIndex() {
        self.pageIndicator.currentPage = currentIndex
    }
    
}

extension PageViewController : UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = viewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard viewControllers.count > previousIndex else {
            return nil
        }
        
        return viewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = viewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = viewControllers.count
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return viewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed) {
            currentIndex = viewControllers.firstIndex(of: pageViewController.viewControllers?.first ?? UIViewController()) ?? 0
            self.setPageIndicatorCurrentIndex()
        }
    }
}
