//
//  ScanViewPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import SwiftDGC
import UIKit
import Combine

protocol ScanView {
    func goTo(segueIdentifier: String, data: Any?)
    func showDocumentTypeView(during time: Double)
    func showAlert(withTitle title: String, withMessage message: String, andAction action: UIAlertAction)
    func showLoading()
    func hideLoading()
    func popToHome()
}

class ScanViewPresenter {
    
    private let scanView: ScanView
    private let premiumManager: PremiumManager
    private let userDataManager: UserDataManager
    private let barcodeVerifier: BarcodeService
    private let notificationCenter: NotificationCenter
    private var cancellables = Set<AnyCancellable>()
    
    init(_ scanView: ScanView, userDataManager: UserDataManager = .sharedInstance, premiumManager: PremiumManager = .shared, barcodeVerifier: BarcodeService = .shared, notificationCenter: NotificationCenter = .default) {
        self.scanView = scanView
        self.premiumManager = premiumManager
        self.userDataManager = userDataManager
        self.barcodeVerifier = barcodeVerifier
        self.notificationCenter = notificationCenter
    }
    
    func proceedToTreatment(with barcode: String, completion: @escaping () -> Void) {
        HCert.publicKeyStorageDelegate = self

        if barcodeVerifier.isValidPassCovid2DDoc(barcode: barcode) {
            self.scanView.goTo(segueIdentifier: userDataManager.shouldShowResultTutorial ? ConstSegue.tutorialResultSegue : ConstSegue.resultSegue, data: barcode)
        } else if HCert.init(from: barcode) != nil {
            self.scanView.goTo(segueIdentifier: userDataManager.shouldShowResultTutorial ? ConstSegue.tutorialResultSegue : ConstSegue.resultSegue, data: barcode)
        } else {
            self.scanView.showDocumentTypeView(during: 3.0)
            completion()
        }
    }
}


extension ScanViewPresenter: PublicKeyStorageDelegate {
   
    
    func fetchOTModeConfiguration(completion: @escaping () -> Void) {
        SyncManager.shared.startLocal()
            .map { true }
            .flatMap(SyncManager.shared.start(force:))
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] result in
                switch result {
                case .failure:
                    self?.scanView.popToHome()
                case .finished:
                    self?.scanView.popToHome()
                }
                
                self?.scanView.hideLoading()
                completion()
            })
            .store(in: &self.cancellables)
    }
    
    func getEncodedPublicKeys(for kid: String) -> [String] {
        guard let certificateString = CertificatesManager.shared.dccCertificates?[kid] else { return [] }
        guard let base64Data = Data(base64Encoded: certificateString) else { return [] }
        guard let base64Decoded = String(data: base64Data, encoding: .utf8) else { return [] }
        return [base64Decoded]
    }
}
