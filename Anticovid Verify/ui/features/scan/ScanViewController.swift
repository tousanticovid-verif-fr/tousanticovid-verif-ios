//
//  ScanViewController.swift
//  Anticovid Verify
//
//

import AVFoundation
import os.log
import UIKit
import SwiftJWT
import SwiftDGC

class ScanViewController : INViewController {
    
    private let resultSegue = "resultSegue"
    private let tutorialResultSegue = "tutorialResultSegue"
    
    // MARK: Properties
    private var captureSession: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var barcodeFrameView: UIView?
    private var captureMetadataOutput: AVCaptureMetadataOutput?
    
    @IBOutlet weak var tutorialView: UIVisualEffectView!
    @IBOutlet weak var documentTypeView: UIVisualEffectView!
    @IBOutlet weak var flashButton: RoundedButton!
    @IBOutlet weak var mireImage: UIImageView!
    
    private var hiddenObserver: NSKeyValueObservation?
    
    // MARK: Lifecycle Methods
    enum FlashStatus {
        case on,off,unavailable
    }
    var flashStatus : FlashStatus = .off {
        didSet {
            if flashStatus == .unavailable {
                flashButton.isHidden = true
                return
            }
            if let device = AVCaptureDevice.default(for: AVMediaType.video){
                do {
                    try device.lockForConfiguration()
                    device.torchMode = flashStatus == .on ? .on : .off
                    if flashStatus == .on {
                        flashButton.setImage(UIImage(named: "flash_on")!, for: .normal)
                    } else {
                        flashButton.setImage(UIImage(named: "flash_off")!, for: .normal)
                    }
                    device.unlockForConfiguration()
                } catch {
                    self.flashStatus = .unavailable
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Scan du 2D-Doc/QR Code"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "questionmark.circle"), style: .plain, target: self, action: #selector(onHelp))
        
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
        else {
            flashStatus = .unavailable
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let index = self.navigationController?.viewControllers.firstIndex(where: { type(of: $0) == TutorialScanViewController.self }) {
            self.navigationController?.viewControllers.remove(at: index)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
            initView()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                DispatchQueue.main.async {
                    if granted {
                        self.initView()
                    } else {
                        let alertController = UIAlertController(title: "Autorisation de la caméra", message: "La fonctionnalité principale de l'application est de scanner les justificatifs de tests COVID-19 ou les certificats de vaccination COVID-19.\nL'accès à la caméra de cet appareil par l'application lui est donc nécessaire pour fonctionner.", preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: "Réglages", style: .default) { (_) -> Void in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                            }
                        }
                        let cancelAction = UIAlertAction(title:"Annuler", style: .default, handler: { (_) -> Void in
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                        alertController.addAction(cancelAction)
                        alertController.addAction(settingsAction)
                        alertController.preferredAction = settingsAction
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
            
            
        }
        
    }
    
    @objc func onHelp() {
        if let vc = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(identifier: "TutorialScanViewController") as? TutorialScanViewController {
            vc.fromHelp = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onFlash(_ sender: Any) {
        if flashStatus == .off {
            flashStatus =  .on
        } else if flashStatus == .on {
            flashStatus = .off
        } else {
            flashStatus =  .on
        }
    }
    
    // MARK: Private Methods
    private func initView() {
        initCapture()
        captureSession?.startRunning()
        setFrame()
    }

    @IBAction func pop(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    private func initCapture() {
        if let captureDevice = AVCaptureDevice.default(for: .video) {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)
                
                captureSession = AVCaptureSession()
                captureSession?.addInput(input)
            } catch {
                os_log("%@",type: .error,"Error while open capture session")
            }
        }
        
        captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput!)
        
        captureMetadataOutput?.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput?.metadataObjectTypes = [AVMetadataObject.ObjectType.dataMatrix, AVMetadataObject.ObjectType.qr]
        
        if captureSession != nil {
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
        }
        
        if videoPreviewLayer != nil {
            view.layer.addSublayer(videoPreviewLayer!)
            self.view.bringSubviewToFront(self.flashButton)
            self.view.bringSubviewToFront(self.tutorialView)
            self.view.bringSubviewToFront(self.documentTypeView)
            self.view.bringSubviewToFront(self.mireImage)
        }
    }
    
    private func setFrame() {
        if let rectOfInterest = videoPreviewLayer?.metadataOutputRectConverted(fromLayerRect: self.view.frame) {
            captureMetadataOutput?.rectOfInterest = rectOfInterest
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == resultSegue, let resultViewController = segue.destination as? ResultContainerViewController, let encoded2DDoc = sender as? String {
            resultViewController.encodedBarcode = encoded2DDoc
            resultViewController.typeBarcode = 1
        }
        
        if segue.identifier == tutorialResultSegue, let vc = segue.destination as? TutorialResultViewController, let encoded2DDoc = sender as? String {
            vc.encodedBarcode = encoded2DDoc
            vc.typeBarcode = 1
        }
    }
}

// MARK: AVCaptureMetadataOutputObjectsDelegate

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            //TODO : error
            return
        }
        
        let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject

        captureSession?.stopRunning()
        if metadataObj != nil && metadataObj!.type == AVMetadataObject.ObjectType.dataMatrix,
           let barcode = metadataObj?.stringValue, isValidPassCovid2DDoc(barcode: barcode) {
            
            if UserData.sharedInstance.shouldShowResultTutorial {
                performSegue(withIdentifier: tutorialResultSegue, sender: metadataObj?.stringValue)
            } else {
                performSegue(withIdentifier: resultSegue, sender: metadataObj?.stringValue)
            }
        } else if metadataObj != nil && metadataObj!.type == AVMetadataObject.ObjectType.qr,
                  let qrcode = metadataObj?.stringValue {
            
            if HCert.init(from: qrcode) != nil {
                if UserData.sharedInstance.shouldShowResultTutorial {
                    performSegue(withIdentifier: tutorialResultSegue, sender: qrcode)
                } else {
                    performSegue(withIdentifier: resultSegue, sender: qrcode)
                }
            } else if let jwt = isValidQrCode(qrcode: qrcode), let expirationDate = jwt.claims.exp, let siren = jwt.claims.siren {
                let calendar = Calendar.current
                let calendarComponents: Set<Calendar.Component> = [Calendar.Component.hour]
                let components = calendar.dateComponents(calendarComponents, from: Date(), to: expirationDate)
                let jours = (components.hour ?? 0) / 24
                
                if UserData.sharedInstance.shouldShowFullResult == false {
                    UserData.sharedInstance.tokenOT = qrcode
                    UserData.sharedInstance.siren = siren
                    
                    let message = jours > 0 ? "L'application a été activée en mode contrôle détaillé pour Opérateur de Transport.\nVotre licence est valable pendant \(jours) jours.\nVous pouvez à tout moment quitter ce mode depuis l'écran paramètres." : "L'application a été activée en mode contrôle détaillé pour Opérateur de Transport.\nVotre licence est valable pendant \(components.hour ?? 0) heures.\nVous pouvez à tout moment quitter ce mode depuis l'écran paramètres."
                    
                    let alertController = UIAlertController(title: "ACTIVATION DU MODE DETAILLE POUR OPERATEUR DE TRANSPORT", message: message, preferredStyle: .alert)
                    let acceptAction = UIAlertAction(title: "Ok", style: .default) { (_) -> Void in
                        UserData.sharedInstance.shouldShowFullResult = true
                        UserData.sharedInstance.shouldShowResultTutorial = true
                        UserData.sharedInstance.expirationDateForOT = expirationDate.timeIntervalSince1970
                        NotificationCenter.default.post(name: ConstNotification.navControllerAppareance, object: nil)
                        self.captureSession?.startRunning()
                    }

                    alertController.addAction(acceptAction)
                    alertController.preferredAction = acceptAction
                
                    self.present(alertController, animated: true, completion: nil)
                } else if UserData.sharedInstance.tokenOT != qrcode {
                    UserData.sharedInstance.tokenOT = qrcode
                    UserData.sharedInstance.siren = siren
                    
                    let message = jours > 0 ? "Nouvelle licence Mode OT prise en compte.\nVotre licence est valable \(jours) jours.\nVous pouvez à tout moment quitter ce mode depuis l'écran paramètres." : "Nouvelle licence Mode OT prise en compte.\nVotre licence est valable \(components.hour ?? 0) heures.\nVous pouvez à tout moment quitter ce mode depuis l'écran paramètres."
                    
                    let alertController = UIAlertController(title: "ACTIVATION DU MODE DETAILLE POUR OPERATEUR DE TRANSPORT", message: message, preferredStyle: .alert)
                    let acceptAction = UIAlertAction(title: "Ok", style: .default) { (_) -> Void in
                        UserData.sharedInstance.shouldShowFullResult = true
                        UserData.sharedInstance.shouldShowResultTutorial = true
                        UserData.sharedInstance.expirationDateForOT = expirationDate.timeIntervalSince1970
                        NotificationCenter.default.post(name: ConstNotification.navControllerAppareance, object: nil)
                        self.captureSession?.startRunning()
                    }
                    alertController.addAction(acceptAction)
                    alertController.preferredAction = acceptAction
                
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    captureSession?.startRunning()
                }
            }
        } else {
            if self.documentTypeView.isHidden {
                self.documentTypeView.isHidden = false
                Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.hideDocumentTypeView), userInfo: nil, repeats: false)
            }
            captureSession?.startRunning()
        }
    }
    
    @objc func hideDocumentTypeView() {
        self.documentTypeView.isHidden = true
     }
    
    private func isValidPassCovid2DDoc(barcode: String) -> Bool {
        if barcode.count < 22 {
            return false
        }
        
        let start = barcode.index(barcode.startIndex, offsetBy: 20)
        let end = barcode.index(barcode.startIndex, offsetBy: 22)
        let documentType = String(barcode[start..<end])
        
        return ConstConf.passCovidDocumentTypes.contains(documentType)
    }
    
    private func isValidQrCode(qrcode: String) -> JWT<INClaims>? {
        guard let publicKey = Data(base64Encoded: ConstConf.publicKey) else {
            return nil
        }
        let jwtVerifier = JWTVerifier.rs256(publicKey: publicKey)
        let jwtDecoder = JWTDecoder(jwtVerifier: jwtVerifier)
        do {
            let jwt = try jwtDecoder.decode(JWT<INClaims>.self, fromString: qrcode)
            let validation = jwt.validateClaims()
            guard validation == .success else {
                return nil
            }
            return jwt.claims.realm_access?.contains(where: { (key: String, value: [String]) in
                return key == "roles" &&
                    value.contains("ROLE_TACV_CONTROL_OT") &&
                    value.contains("ROLE_VERIFY_CONTROL_2DDOC_L1") &&
                    value.contains("ROLE_VERIFY_CONTROL_2DDOC_B2")
            }) ?? false ? jwt : nil
        }
        catch {
            os_log("%@",type: .error, error.localizedDescription)
        }
        
        return nil
    }
}
