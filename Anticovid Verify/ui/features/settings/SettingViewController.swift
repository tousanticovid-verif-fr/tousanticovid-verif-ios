//
//  SettingViewController.swift
//  Anticovid Verify
//
//

import UIKit
import Combine
import OSLog

class SettingViewController: INViewController {
    
    @IBOutlet weak var showScanTutorialSwitch: UISwitch!
    @IBOutlet weak var showResultTutorialSwitch: UISwitch!
    @IBOutlet weak var showFullResultSwitch: UISwitch!
    @IBOutlet weak var logoIN : UIImageView!
    
    @IBOutlet weak var titleLabel: INLabel!
    @IBOutlet weak var descriptionLabel: INLabel!
    @IBOutlet weak var fullResultStackView: UIStackView!
    @IBOutlet weak var infosLabel: INLabel!
    @IBOutlet weak var warningLabel: INLabel!
    @IBOutlet weak var syncLabel: INLabel!
    @IBOutlet weak var syncButton: RoundedButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var cancellables = Set<AnyCancellable>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let timeinterval = TimeInterval(PremiumManager.shared.expirationDateForOT)
        let expirationDate = Date(timeIntervalSince1970: timeinterval)
        let calendar = Calendar.current
        let calendarComponents: Set<Calendar.Component> = [Calendar.Component.day]
        let components = calendar.dateComponents(calendarComponents, from: Date(), to: expirationDate)
        infosLabel.text = "Votre licence expirera dans \(components.day ?? 0) jours."
            
        hideViews(PremiumManager.shared.tokenOT.isEmpty)
        
        showScanTutorialSwitch.setOn(UserDataManager.sharedInstance.shouldShowScanTutorial, animated: true)
        showResultTutorialSwitch.setOn(UserDataManager.sharedInstance.shouldShowResultTutorial, animated: true)
        showFullResultSwitch.setOn(!PremiumManager.shared.tokenOT.isEmpty, animated: true)
        if ConstConf.filelogEnabled {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onLogo))
            logoIN.isUserInteractionEnabled = true
            logoIN.addGestureRecognizer(tapGestureRecognizer)
        }
        updateSyncDate()
    }
    
    func updateSyncDate(){
        self.syncLabel.text = SyncManager.shared.lastSyncDate?.toFormattedString("dd/MM/yyyy HH:mm:ss")
        syncButton.backgroundColor = syncButton.isEnabled ? .goldIn : .secondSubtitle
    }
    
    @IBAction func syncButtonTapped(_ sender: Any) {
        activityIndicator.startAnimating()
        
        SyncManager.shared.start(force: true)
            .subscribe(on: DispatchQueue.main)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] result in
                self?.activityIndicator.stopAnimating()

                switch result {
                case .failure:
                    break
                    //os_log("%@",type: .error, "error: \(error)")
                case .finished:
                    self?.syncButton.isEnabled = false
                    self?.updateSyncDate()
                }
            })
            .store(in: &cancellables)
    }
    
    @IBAction func shouldShowScanTutorialChange(_ sender: Any) {
        if let choice = sender as? UISwitch {
            UserDataManager.sharedInstance.shouldShowScanTutorial = choice.isOn
        }
    }
    
    @IBAction func shouldShowResultTutorialChange(_ sender: Any) {
        if let choice = sender as? UISwitch {
            UserDataManager.sharedInstance.shouldShowResultTutorial = choice.isOn
        }
    }
    
    @IBAction func activateOTModeSwitchValueChanged(_ sender: Any) {
        if let _ = sender as? UISwitch {
            let alertController = UIAlertController(title: "Désactivation du mode TAC Verif +", message: "Attention, cette action n'est reversible que par un processus de réactivation.", preferredStyle: .alert)
            let acceptAction = UIAlertAction(title: "OK", style: .destructive) { (_) -> Void in
                
                PremiumManager.shared.resetPremium()

                self.showResultTutorialSwitch.setOn(UserDataManager.sharedInstance.shouldShowResultTutorial, animated: true)
                
                self.hideViews(!PremiumManager.shared.isPremium)
                
                // handle UI changes on navigation bar
                NotificationCenter.default.post(name: ConstNotification.navControllerAppareance, object: nil)
                UINavigationController.updateNavigationBarTheme(navigationBar: self.navigationController?.navigationBar)
            }
            
            let deniedAction = UIAlertAction(title:"Annuler", style: .cancel, handler: { (_) -> Void in
                self.showFullResultSwitch.setOn(PremiumManager.shared.isPremium, animated: true)
            })
            alertController.addAction(deniedAction)
            alertController.addAction(acceptAction)
            alertController.preferredAction = acceptAction
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTouchSystemSetting() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url)
        }
    }
    @objc func onLogo(sender:UIView){
        FileLogger.shared.send(from: self)
        FileLogger.shared.clean()

    }
}

private extension SettingViewController {
    func hideViews(_ value: Bool) {
        [titleLabel, descriptionLabel,
         fullResultStackView,infosLabel,
         warningLabel]
            .forEach { $0.isHidden = value }
    }
}
