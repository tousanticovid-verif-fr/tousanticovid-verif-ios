//
//  LocalStatisticPresenter.swift
//  Anticovid Verify
//
//

import Foundation

typealias HistoryData = (date: Date, numberOfScan: Int, double: Int)

typealias ChartData = (numberOfScan: Double, numberOfDuplicate: Double)

protocol LocalStatisticView {
    func updateHeader(with dayStatistic: LocalStatistic?)
    func updateHistory(withData history: [HistoryData])
    func updateChart(withData chartData: [ChartData])
}

class LocalStatisticPresenter {
    
    let statService: StatService
    
    let localStatisticView: LocalStatisticView
    
    init(_ localStatisticView: LocalStatisticView, statService: StatService = .shared) {
        self.statService = statService
        self.localStatisticView = localStatisticView
    }
    
    func fetchStatistics() {
        var todayData: LocalStatistic?
        var historyData = [HistoryData]()
        var chartData = [ChartData]()

        let statisticsResult = statService.readStat().1.sorted(by: { $0.date!.compare($1.date!) == .orderedDescending })
        let fifteenLastDays = Date.getDates(forLastNDays: 15)
        
        fifteenLastDays.forEach { date in
            guard let statisticsResultForSpecificDay = statisticsResult.filter({ Calendar.current.isDate($0.date!, inSameDayAs: date) }).first else {
                historyData.append((date, 0, 0))
                chartData.append(ChartData(numberOfScan: 0, numberOfDuplicate: 0))
                return
            }
            if !Calendar.current.isDate(Date(), inSameDayAs: statisticsResultForSpecificDay.date!) {
                let history = HistoryData(
                    date: statisticsResultForSpecificDay.date!,
                    numberOfScan: statisticsResultForSpecificDay.calculateNumberOfScan(),
                    double: statisticsResultForSpecificDay.double.toInt()
                )
                historyData.append(history)
            } else {
                todayData = statisticsResultForSpecificDay
            }
            
            chartData.append((ChartData(numberOfScan: statisticsResultForSpecificDay.calculateNumberOfScan().toDouble(), numberOfDuplicate: statisticsResultForSpecificDay.double)))
        }

        localStatisticView.updateHeader(with: todayData)
        localStatisticView.updateHistory(withData: historyData)
        localStatisticView.updateChart(withData: chartData.reversed())
    }
}
