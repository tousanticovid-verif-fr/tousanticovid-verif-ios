//
//  WkWebViewViewContoller.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit
import WebKit

enum WkWebViewViewContollerErrors: Error {
    case dataNotInitialized
}


enum WkWebViewDataType {
    case URL, HTLM
}

struct WkWebViewData {
    let value: String
    let type: WkWebViewDataType
}

class WkWebViewViewContoller: UIViewController {
    
    @IBOutlet weak var wkWebView: WKWebView!
    
    var wkWebViewData: WkWebViewData!
    var transparent: Bool = false

    private var contentStartedLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        try? initialized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadContent()
        
        if transparent {
            self.view.backgroundColor = .clear
            self.wkWebView.backgroundColor = .clear
            self.wkWebView.scrollView.backgroundColor = .clear
            self.wkWebView.isOpaque = false
        }
        
        self.wkWebView.scrollView.showsVerticalScrollIndicator = false
    }
    
    private func initialized() throws {
        if wkWebViewData == nil {
            throw WkWebViewViewContollerErrors.dataNotInitialized
        }
    }
    
    private func loadContent() {
        guard !contentStartedLoading else { return }
        
        switch wkWebViewData.type {
        case .URL:
            let url = URLRequest(url: URL(string: wkWebViewData.value)!)
            wkWebView.load(url)
        case .HTLM:
            wkWebView.loadHTMLString(wkWebViewData.value, baseURL: nil)
        }
        
        contentStartedLoading = true
    }
}
