//
//  HomeViewPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import Combine
import UIKit

protocol PremiumView {
    func showSection(with goInData: GoInData?)
    func showSection(with goOutData: GoOutData?)
    func hideOtConfigView(with displayOtConfigview: Bool)
    func refreshUI(with goOutData: GoOutData?)
    func refreshUI(with goInData: GoInData?)
    func refreshFavorites(with countries: [Country])
    func openSelectCountries(with configuration: SelectCountriesConfiguration)
    func displayDatePicker()
    func initModeSelectedSegmentedControl(with indexSelected: Int)
    func refreshUIForCheckDate(with labelString: String, textColor: UIColor)
    func displayAlertControllerForResetDate()

    func configUIElements(modeIndexSelected: Int,
                          modeSpecificIsNotSelected: Bool,
                          OTModeExpireInLessThan20Days: Bool,
                          OTModeExpirationLabel: String,
                          messageOTMode: String?,
                          hideOTView: Bool)
    
}

class PremiumPresenter {
    
    private let premiumView: PremiumView
    private let premiumManager: PremiumManager
    
    private let userDataManager: UserDataManager
    
    private(set) var otViewConfigIsDisplayed = false
    
    private let otDataManager : OtDataManager
    
    private let syncManager: SyncManager
    
    private let uiDataManager: UIDataManager

    private var chooseCurrentCheckDate: Bool = true
    private var cancellables = [AnyCancellable]()

    init(_ premiumView: PremiumView, premiumManager: PremiumManager = .shared, userDataManager: UserDataManager = .sharedInstance, otDataManager: OtDataManager = .shared, syncManager: SyncManager = .shared, uiDataManager: UIDataManager = .shared) {
        self.premiumView = premiumView
        self.premiumManager = premiumManager
        self.userDataManager = userDataManager
        self.otDataManager = otDataManager
        self.syncManager = syncManager
        self.uiDataManager = uiDataManager
    }
    
    func handleDidload() {
        subscribeToSwitchOTModeEvent()
    }
    
    func handleWillAppear() {
        let modeSelected = premiumManager.modeSelected
        let modes = premiumManager.modesAvailable()
        let index = modes.firstIndex(of: modeSelected) ?? 0
        
        premiumView.configUIElements(
            modeIndexSelected: index,
            modeSpecificIsNotSelected: premiumManager.tokenOT.isEmpty,
            OTModeExpireInLessThan20Days: OTModeExpireInLessThan20Days(),
            OTModeExpirationLabel: OTModeExpirationLabel(),
            messageOTMode: uiDataManager.messageOtModeFR,
            hideOTView: modeSelected != .OT
        )
        
        changeOTSectionDisplayed(with: otDataManager.state)
    }

    func canScanBarcode() -> Bool {
        return syncManager.actualAppSyncRemindStep() != .high
    }
    
    func changeOTSectionDisplayed(with otState: OtState) {
        self.otDataManager.state = otState
        if otState == .goInState {
            refreshUIForNewCheckDate(with: self.otDataManager.goInData.currentCheckDate)
            premiumView.showSection(with: otDataManager.goInData)
            premiumView.refreshUI(with: otDataManager.goInData)
        } else {
            refreshUIForNewCheckDate(with: self.otDataManager.goOutData.currentCheckDate)
            premiumView.showSection(with: otDataManager.goOutData)
            premiumView.refreshUI(with: otDataManager.goOutData)
        }
    }
    
    func updateCountrieConfiguration(with configuration: SelectCountriesConfiguration) {
        switch configuration.selectCountryMode {
        case .selectOneCountry:
            if let selectCountry = configuration.countries.last {
                otDataManager.goOutData.departureCountry = selectCountry
                otDataManager.goInData.destinationCountry = selectCountry
                if(otDataManager.state == .goOutState) {
                    self.premiumView.refreshUI(with: otDataManager.goOutData)
                    self.premiumView.refreshFavorites(with: otDataManager.goOutData.favoritesCountries )
                } else {
                    self.premiumView.refreshUI(with: otDataManager.goInData)
                    self.premiumView.refreshFavorites(with: otDataManager.goInData.favoritesCountries )
                }
            }
        case .selectCountries:
            if(otDataManager.state == .goOutState) {
                otDataManager.goOutData.favoritesCountries = configuration.countries
                self.premiumView.refreshUI(with: otDataManager.goOutData)
                self.premiumView.refreshFavorites(with: otDataManager.goOutData.favoritesCountries )
            } else {
                otDataManager.goInData.favoritesCountries = configuration.countries
                self.premiumView.refreshUI(with: otDataManager.goInData)
                self.premiumView.refreshFavorites(with: otDataManager.goInData.favoritesCountries )
            }
        }
        self.otDataManager.save(goInData: otDataManager.goInData, goOutData: otDataManager.goOutData)
    }
    
    func displaySelectNationalCountryOrRegion() {
        var country = [Country]()
        var exceptCountry = [Country]()
        
        if otDataManager.state == .goInState {
            country.append(otDataManager.goInData.destinationCountry)
            exceptCountry.append(contentsOf: otDataManager.goInData.favoritesCountries)
        } else {
            country.append(otDataManager.goOutData.departureCountry)
            exceptCountry.append(contentsOf: otDataManager.goOutData.favoritesCountries)
        }
        
        self.premiumView.openSelectCountries( with: SelectCountriesConfiguration(
                selectCountryMode: .selectOneCountry(isNational: true),
                countries: country,
                exceptCountry: exceptCountry,
                mode: otDataManager.state
            )
        )
    }
    
    func displaySelectCountries() {
        let exceptCountry = otDataManager.state == .goOutState ? [otDataManager.goOutData.departureCountry] : [otDataManager.goInData.destinationCountry]
        let favoritesCountries = otDataManager.state == .goOutState ? otDataManager.goOutData.favoritesCountries : otDataManager.goInData.favoritesCountries
        self.premiumView.openSelectCountries(
            with: SelectCountriesConfiguration(
                selectCountryMode: .selectCountries(numberMax: 4, isNational: false),
                countries: favoritesCountries,
                exceptCountry: exceptCountry,
                mode: otDataManager.state
            )
        )
    }
    
    func checkStartDatePicker() {
        if(otDataManager.state == .goInState) {
            if (otDataManager.goInData.currentCheckDate != .actual) {
                self.premiumView.displayAlertControllerForResetDate()
            } else {
                self.premiumView.displayDatePicker()
            }
        } else {
            if (otDataManager.goOutData.currentCheckDate != .actual) {
                self.premiumView.displayAlertControllerForResetDate()
            } else {
                self.premiumView.displayDatePicker()
            }
        }
    }

    func updateCurrentCheckDate(with checkDate: DateState) {
        if otDataManager.state == .goInState {
            otDataManager.goInData.currentCheckDate = checkDate
            self.premiumView.refreshUI(with: otDataManager.goInData)
        } else {
            otDataManager.goOutData.currentCheckDate = checkDate
            self.premiumView.refreshUI(with: otDataManager.goOutData)
        }
        refreshUIForNewCheckDate(with: checkDate)
        self.otDataManager.save(goInData: otDataManager.goInData, goOutData: otDataManager.goOutData)
    }
    
    func updateDepartureCountry(with departureNational: Country) {
        guard otDataManager.state == .goInState else { return }

        var currentGoInData = otDataManager.goInData
        currentGoInData.destinationCountry = departureNational
        premiumView.refreshUI(with: currentGoInData)
        self.otDataManager.save(goInData: currentGoInData, goOutData: otDataManager.goOutData)
    }
    
    func refreshUIForNewCheckDate(with checkDate: DateState) {
        if otDataManager.state == .goOutState {
            if checkDate == .actual {
                self.premiumView.refreshUIForCheckDate(with: "Date et heure actuelle", textColor: .goldIn)
            } else {
                self.premiumView.refreshUIForCheckDate(with: checkDate.getDate().fullDateFormattedForHuman(), textColor: .black)
            }
        } else {
            if checkDate == .actual {
                self.premiumView.refreshUIForCheckDate(with: "Date et heure actuelle", textColor: .goldIn)
            } else {
                self.premiumView.refreshUIForCheckDate(with: checkDate.getDate().fullDateFormattedForHuman(), textColor: .black)
            }
        }
    }
    
    func resetCheckDateForOtData() {
        otDataManager.goInData.currentCheckDate = .actual
        otDataManager.goOutData.currentCheckDate = .actual
        self.otDataManager.save(goInData: otDataManager.goInData, goOutData: otDataManager.goOutData)
    }
    
    func changeSelectedMode(with segmentedControlIndexSelected: Int) {
        let modes = premiumManager.modesAvailable()
        let newSelectedMode = modes[segmentedControlIndexSelected]
       
        premiumManager.modeSelected = newSelectedMode
        premiumView.hideOtConfigView(with: newSelectedMode != .OT)
        
        premiumView.configUIElements(
            modeIndexSelected: segmentedControlIndexSelected,
            modeSpecificIsNotSelected: premiumManager.tokenOT.isEmpty,
            OTModeExpireInLessThan20Days: OTModeExpireInLessThan20Days(),
            OTModeExpirationLabel: OTModeExpirationLabel(),
            messageOTMode: uiDataManager.messageOtModeFR,
            hideOTView: newSelectedMode != .OT
        )
        
        NotificationCenter.default.post(name: ConstNotification.navControllerAppareance, object: nil)
    }
}

private extension PremiumPresenter {
    func subscribeToSwitchOTModeEvent() {
        NotificationCenter.default.publisher(for: ConstNotification.navControllerAppareance,
                                                object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let self = self else { return }

                let modes = self.premiumManager.modesAvailable()
                let modeSelected = self.premiumManager.modeSelected
                let index = modes.firstIndex(of: modeSelected) ?? 0
                
                self.premiumView.configUIElements(
                    modeIndexSelected: index,
                    modeSpecificIsNotSelected: self.premiumManager.tokenOT.isEmpty,
                    OTModeExpireInLessThan20Days: self.OTModeExpireInLessThan20Days(),
                    OTModeExpirationLabel: self.OTModeExpirationLabel(),
                    messageOTMode: self.uiDataManager.messageOtModeFR,
                    hideOTView: modeSelected != .OT)
            }
            .store(in: &cancellables)
    }
    
   
    
    func OTModeExpirationLabel() -> String {
        let hours = OTModeExpiration(in: .hour)
        let days = hours / 24
        
        return days > 0 ?
        "Le mode TAC Verif + va expirer dans \(days) jours." :
        "Le mode TAC Verif + va expirer dans \(hours) heures."
    }
    
    func OTModeExpireInLessThan20Days() -> Bool {
        guard premiumManager.isPremium else { return false }
        
        return OTModeExpiration(in: .day) <= 20
    }
    
    func OTModeExpiration(in component: Calendar.Component) -> Int {
        let timeinterval = TimeInterval(premiumManager.expirationDateForOT)
        let expirationDate = Date(timeIntervalSince1970: timeinterval)
        let calendar = Calendar.current
        let calendarComponents: Set<Calendar.Component> = [component]
        let components = calendar.dateComponents(calendarComponents, from: Date(), to: expirationDate)
        
        return components.value(for: component) ?? 0
    }
}
