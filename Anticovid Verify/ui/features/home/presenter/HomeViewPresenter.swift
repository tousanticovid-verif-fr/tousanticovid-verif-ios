//
//  HomeViewPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit
import Combine

protocol HomeView {
    func displayCanScan(_ value: Bool)
    func refreshScanButton()
    func swithToPassVaccinal(_ value:Bool)
    func showHealthPassUI(description: String)
    func showVaccinationPassUI(description: String)
    func showVaccinalPassFeature(_ value:Bool)
}

class HomeViewPresenter {
    private var cancellables = Set<AnyCancellable>()

    private let homeView: HomeView
    private let syncManager : SyncManager
    private let premiumManager : PremiumManager
    private let userdataManager : UserDataManager

    init(_ homeView: HomeView, syncManager: SyncManager = .shared,
         premiumManager: PremiumManager = .shared,
         userDataManager: UserDataManager = .sharedInstance) {
        self.homeView = homeView
        self.syncManager = syncManager
        self.premiumManager = premiumManager
        self.userdataManager = userDataManager
    }
    
    func handleWillAppear() {
        subscribeToRefeshCertificatesEvent()
        subscribeToEnterFromBackground()
        
        homeView.displayCanScan(self.syncManager.actualAppSyncRemindStep() != .high)
        homeView.refreshScanButton()
        prepareViewPassSelected()
        refreshScanFeature()
    }

    func canScanBarcode() -> Bool {
        return syncManager.actualAppSyncRemindStep() != .high
    }
    
    func refreshScanFeature() {
        var isPassVaccinalFeatureActivated = false
        if case .tacVerif = premiumManager.modeSelected,
            let startDate = RulesMotorRepository.shared.getMotorRules()?.vaccinePassStartDate {
            isPassVaccinalFeatureActivated = Date() >= startDate
        }
        
        homeView.showVaccinalPassFeature(isPassVaccinalFeatureActivated)            
    }
    
    func scanButtonTitle() -> NSAttributedString {
        let shouldRefreshCertificates = syncManager.actualAppSyncRemindStep() == .high
        let scanImpossible = "Scan impossible. Synchronisation nécessaire"
        let scanPossible = "Scanner le code 2D-DOC ou QR Code"
        let attributedString = NSMutableAttributedString()
            .appendText(shouldRefreshCertificates ? scanImpossible : scanPossible,
                        font: UIFont(name: "Avenir-Heavy", size: 17),
                        color: .blueIn)
        
        switch premiumManager.modeSelected {
        case .tacVerifPlus:
            attributedString.appendText("\nMode détaillé pour Professionnels de Santé",
                                        font: UIFont(name: "Avenir-Heavy", size: 12),
                                        color: .blueIn)
        case .OT:
            attributedString.appendText("\nMode détaillé pour Opérateurs de Transport",
                                        font: UIFont(name: "Avenir-Heavy", size: 12),
                                        color: .blueIn)
        case .tacVerif(let passSelected, _):
            switch passSelected {
            case .vaccine:
                attributedString.appendText("\nPass vaccinal",
                                            font: UIFont(name: "Avenir-Heavy", size: 12),
                                            color: .blueIn)
            case .health:
                attributedString.appendText("\nPass sanitaire",
                                            font: UIFont(name: "Avenir-Heavy", size: 12),
                                            color: .blueIn)
            }
        }
        
        
        return attributedString
    }
    
    func subscribeToRefeshCertificatesEvent() {
        NotificationCenter.default.publisher(for: ConstNotification.updateBannerSyncApp, object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.homeView.displayCanScan(self.syncManager.actualAppSyncRemindStep() != .high)
                self.refreshScanFeature()
            }
            .store(in: &cancellables)
    }
    
    func subscribeToEnterFromBackground() {
        NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification, object: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.prepareViewPassSelected()
                self.refreshScanFeature()
            }
            .store(in: &cancellables)
    }
    
    func switchToPassVaccinal(_ value: Bool, shouldShowResultTutorial: Bool = false) {
        userdataManager.shouldShowResultTutorial = shouldShowResultTutorial ? true : userdataManager.shouldShowResultTutorial
        premiumManager.modeSelected = .tacVerif(passSelected: value ? .vaccine : .health, at: Date())
        
        displaySwitchToPassVaccinal(value)
    }
     
    func prepareViewPassSelected() {
        let configs = premiumManager.modesAvailable()
        let index = configs.firstIndex(of: .tacVerifAny)
        let tacVerifConfig = configs[index!]
                
        switch tacVerifConfig {
        case .tacVerif(let passSelected, _):
            homeView.swithToPassVaccinal(passSelected == .vaccine)
            displaySwitchToPassVaccinal(passSelected == .vaccine)
        default:
            break
        }
    }
}

private extension HomeViewPresenter {
    func displaySwitchToPassVaccinal(_ value: Bool, shouldShowResultTutorial: Bool = false) {
        let description = value ?
        UIDataManager.shared.uiLabels?.vaccine.description.fr :
        UIDataManager.shared.uiLabels?.health.description.fr
        
        if value {
            homeView.showVaccinationPassUI(description: description ?? "")
        } else {
            homeView.showHealthPassUI(description: description ?? "")
        }
    }
}
