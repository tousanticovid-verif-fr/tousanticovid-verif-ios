//
//  ViewController.swift
//  Anticovid Verify
//
//

import UIKit

class PremiumViewController: UIViewController {
    
    weak var homeVC : HomeViewController?
    
    @IBOutlet private weak var messageOTModeLabel: UILabel!
    @IBOutlet private weak var otExpirationLabel: UILabel!
    @IBOutlet weak var currentStateIcon: UIImageView!
    @IBOutlet weak var otExpirationStack: UIStackView!
    @IBOutlet weak var modeSegmentedControl: UISegmentedControl!
    
    @IBAction func changeSelectedModeSegmentedControl(_ sender: Any) {
        self.premiumPresenter.changeSelectedMode(with: modeSegmentedControl.selectedSegmentIndex)
    }
    
    @IBOutlet weak var otConfigSegmentedControl: UISegmentedControl!
    
    @IBAction func changeOTConfigSegmentedControl(_ sender: Any) {
        changeOtViewState()
    }

    //Destination
    @IBOutlet weak var destinationCountryView: UIView!
    @IBOutlet weak var departureCountryLabel: UILabel!
    @IBOutlet weak var destinationCountryLabel: UILabel!
    
    //Favorite
    @IBOutlet weak var favoriteView: UIView!
    @IBOutlet weak var favoriteLabel: UILabel!
    @IBOutlet weak var favoriteStackView: UIStackView!
    @IBOutlet weak var addFavoriteButton: UIButton!
    @IBOutlet weak var addButtonFavoriteIcon: UIImageView!
    @IBOutlet weak var firstFavoriteCountry: UIImageView!
    @IBOutlet weak var secondFavoriteCountry: UIImageView!
    @IBOutlet weak var thirdFavoriteCountry: UIImageView!
    @IBOutlet weak var fourthFavoriteCountry: UIImageView!
    
    @IBOutlet weak var firstFavoriteCountryLabel: UILabel!
    @IBOutlet weak var secondFavoriteCountryLabel: UILabel!
    @IBOutlet weak var thirdFavoriteCountryLabel: UILabel!
    @IBOutlet weak var fourthFavoriteCountryLabel: UILabel!
    
    @IBOutlet weak var firstFavoriteCountryStackView: UIStackView!
    @IBOutlet weak var secondFavoriteCountryStackView: UIStackView!
    @IBOutlet weak var thirdFavoriteCountryStackView: UIStackView!
    @IBOutlet weak var fourthFavoriteCountryStackView: UIStackView!
    
    var favoritesImageView = [UIImageView]()
    var favoritesLabel = [UILabel]()
    var favoritesStackView = [UIStackView]()
    
    @IBAction func addFavoriteCountry(_ sender: Any) {
        self.displaySelectCountries()
    }
    
    //Departure
    @IBOutlet weak var departureCountryView: UIView!
    @IBOutlet weak var selectNationalStackView: UIStackView!
    
    //Check date
    @IBOutlet weak var currentDateView: UIView!
    @IBOutlet weak var currentDateLabel: UILabel!
    
    @IBOutlet weak var globalStackView: UIStackView!
    
    var premiumPresenter: PremiumPresenter!
    
    final let barcodeSegue = "barcodeSegue"
    final let privacySegue = "privacySegue"
    final let tutorialScanSegue = "tutorialScanSegue"
    final let otherSegue = "otherSegue"
    final let datePickerSegue = "datePickerSegue"
    
    var datePickerConfiguration: DatePickerConfiguration!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        premiumPresenter = PremiumPresenter(self)
        self.premiumPresenter.resetCheckDateForOtData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Vérification"
        self.favoritesImageView = [firstFavoriteCountry, secondFavoriteCountry, thirdFavoriteCountry, fourthFavoriteCountry]
        self.favoritesLabel = [firstFavoriteCountryLabel, secondFavoriteCountryLabel, thirdFavoriteCountryLabel, fourthFavoriteCountryLabel]
        self.favoritesStackView = [firstFavoriteCountryStackView, secondFavoriteCountryStackView, thirdFavoriteCountryStackView, fourthFavoriteCountryStackView]
        self.otExpirationStack.isHidden = true
        [0...modeSegmentedControl.numberOfSegments - 1].forEach { _ in
            modeSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.blueIn], for: .normal)
        }
        [0...otConfigSegmentedControl.numberOfSegments - 1].forEach { _ in
            otConfigSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.blueIn], for: .normal)
        }
        configureInteraction()
        self.premiumPresenter.handleDidload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.premiumPresenter.handleWillAppear()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == datePickerSegue) {
            if let datePickerVC = segue.destination as? DatePickerViewController {
                datePickerVC.datePickerProtocol = self
            }
        }
    }
}

// MARK: IBActions
private extension PremiumViewController {

    private func configureInteraction() {
        let nationalDepartureTap = UITapGestureRecognizer(target: self, action: #selector(self.selectDestinationCountry))
        let destinationCountryTap = UITapGestureRecognizer(target: self, action: #selector(self.selectDestinationCountry))
        let checkDateTap = UITapGestureRecognizer(target: self, action: #selector(self.startDatePicker))
        let favoriteCountriesTap = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectCountries))
        self.selectNationalStackView.addGestureRecognizer(nationalDepartureTap)
        self.destinationCountryView.addGestureRecognizer(destinationCountryTap)
        self.currentDateView.addGestureRecognizer(checkDateTap)
        self.favoriteStackView.addGestureRecognizer(favoriteCountriesTap)
        self.departureCountryView.addGestureRecognizer(nationalDepartureTap)
    }
    
    @objc func selectDestinationCountry() {
        self.premiumPresenter.displaySelectNationalCountryOrRegion()
    }
    
    @objc func displaySelectCountries() {
        self.premiumPresenter.displaySelectCountries()
    }

    func changeOtViewState() {
        let index = otConfigSegmentedControl.selectedSegmentIndex
        let state: OtState = index == 0 ? .goOutState : .goInState
        self.premiumPresenter.changeOTSectionDisplayed(with: state)
    }
    
    @objc func startDatePicker() {
        self.premiumPresenter.checkStartDatePicker()
    }
}
extension PremiumViewController: DatePickerProtocol {
    func saveNewCheckDate(with newDate: DateState) {
        self.premiumPresenter.updateCurrentCheckDate(with: newDate)
        self.premiumPresenter.changeOTSectionDisplayed(with: OtDataManager.shared.state)
    }
}
extension PremiumViewController: SelectCountriesDelegate {
    
    func closeResult(with configuration: SelectCountriesConfiguration) {
        self.premiumPresenter.updateCountrieConfiguration(with: configuration)
    }
}
extension PremiumViewController: PremiumView {
    func openSelectCountries(with configuration: SelectCountriesConfiguration) {
        SelectCountriesManager().openSelectedCountries(
            selectedCountryDelegate: self,
            selectCountriesConfiguration: configuration
        )
    }
    func configUIElements(modeIndexSelected: Int, modeSpecificIsNotSelected: Bool,
                          OTModeExpireInLessThan20Days: Bool,
                          OTModeExpirationLabel: String,
                          messageOTMode: String?, hideOTView: Bool) {
        modeSegmentedControl.isHidden = modeSpecificIsNotSelected
        initModeSelectedSegmentedControl(with: modeIndexSelected)
        hideOtConfigView(with: hideOTView)
        otExpirationLabel.text = OTModeExpirationLabel
        otExpirationStack.isHidden = !OTModeExpireInLessThan20Days
        messageOTModeLabel.text = messageOTMode
        messageOTModeLabel.isHidden = messageOTMode == nil
        
        homeVC?.refreshScanButton()
        homeVC?.refreshPassFeature()
    }
    
   
    func refreshFavorites(with countries: [Country]) {
        for indexFavoris in 0..<countries.count {
            favoritesImageView[indexFavoris].isHidden = false
            favoritesLabel[indexFavoris].isHidden = false
            favoritesStackView[indexFavoris].isHidden = false
            favoritesImageView[indexFavoris].image = countries[indexFavoris].getFlag()
            favoritesLabel[indexFavoris].text = countries[indexFavoris].shortDisplayName.uppercased()
        }
        
        for index in countries.count..<favoritesImageView.count {
            favoritesImageView[index].isHidden = true
            favoritesLabel[index].isHidden = true
            favoritesStackView[index].isHidden = true
        }
        
        addButtonFavoriteIcon.isHidden = countries.count == favoritesImageView.count
        favoriteStackView.isHidden = countries.isEmpty
        addFavoriteButton.isHidden = !countries.isEmpty
    }
    
    func hideOtConfigView(with displayOtConfigView: Bool) {
        self.globalStackView.isHidden = displayOtConfigView
    }
    
    func showSection(with goInData: GoInData?) {
        self.currentStateIcon.image = UIImage(named: "airplane_arrival")
        self.destinationCountryView.isHidden = true
        self.favoriteLabel.text = "Pays / région de départ"
        self.favoriteView.isHidden = goInData?.favoritesCountries.isEmpty ?? true
        self.addFavoriteButton.isHidden =  (goInData != nil && !goInData!.favoritesCountries.isEmpty )
        self.addFavoriteButton.setTitle("Ajouter un ou plusieurs pays de départ", for: .normal)
        self.departureCountryView.isHidden = false
        self.destinationCountryLabel.text = goInData?.destinationCountry.name ?? ""
        self.refreshFavorites(with: goInData?.favoritesCountries ?? [])
        
        otConfigSegmentedControl.selectedSegmentIndex = 1
    }
    
    func showSection(with goOutData: GoOutData?) {
        self.currentStateIcon.image = UIImage(named: "airplane_departure")
        self.destinationCountryView.isHidden = false
        self.destinationCountryView.backgroundColor = .white
        self.destinationCountryLabel.text = goOutData?.departureCountry.name ?? ""
        self.favoriteLabel.text = "Pays / région de destination"
        self.favoriteView.isHidden = goOutData?.favoritesCountries.isEmpty ?? true
        self.addFavoriteButton.isHidden = goOutData != nil && !goOutData!.favoritesCountries.isEmpty
        self.addFavoriteButton.setTitle("Ajouter une ou plusieurs destinations", for: .normal)
        self.departureCountryView.isHidden = true
        self.refreshFavorites(with: goOutData?.favoritesCountries ?? [])
        
        otConfigSegmentedControl.selectedSegmentIndex = 0
    }
    
    func refreshUI(with goOut: GoOutData?) {
        self.departureCountryLabel.text = goOut?.departureCountry.name ?? ""
        let hasFavorite = goOut?.favoritesCountries.isEmpty ?? true
        self.favoriteView.isHidden = hasFavorite
        self.addFavoriteButton.isHidden = !hasFavorite
    }
    
    func refreshUI(with goInData: GoInData?) {
        self.destinationCountryLabel.text = goInData?.destinationCountry.name ?? ""
        let hasFavorite = goInData?.favoritesCountries.isEmpty ?? true
        self.addFavoriteButton.isHidden = !hasFavorite
        self.favoriteView.isHidden = hasFavorite
    }
    
    func displayDatePicker() {
        self.performSegue(withIdentifier: datePickerSegue, sender: nil)
    }
    
    func initModeSelectedSegmentedControl(with indexSelected: Int) {
        modeSegmentedControl.selectedSegmentIndex = indexSelected
    }

    func refreshUIForCheckDate(with labelString: String, textColor: UIColor) {
        self.currentDateLabel.text = labelString
        self.currentDateLabel.textColor = textColor
    }
    
    func displayAlertControllerForResetDate() {
        let alert = UIAlertController(
            title: "Date de contrôle",
            message:"Voulez-vous définir la date et heure de contrôle comme étant la date du téléphone ?",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Non", style: .cancel) { _ in
            self.displayDatePicker()
            })
        
        alert.addAction(UIAlertAction(title: "Oui", style: .default) { _ in
            self.saveNewCheckDate(with: .actual)
        })

        self.present(alert, animated: true, completion: nil)
    }
}
