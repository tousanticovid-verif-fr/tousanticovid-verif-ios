//
//  DatePickerViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

struct DatePickerConfiguration {
    var currentCheckDate: Date
}

public protocol DatePickerProtocol {
    func saveNewCheckDate(with newDate: DateState)
}

class DatePickerViewController: UIViewController {
    
    var datePickerProtocol: DatePickerProtocol!
    
    var newDate: DateState = .actual

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(datePickerProtocol: DatePickerProtocol) {
        self.datePickerProtocol = datePickerProtocol
        super.init(nibName: nil, bundle: nil)
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        self.datePickerProtocol.saveNewCheckDate(with: newDate)
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureDatePicker()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        containerView.layer.cornerRadius = 10
    }
    
    private func configureDatePicker() {
        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .inline
        } else if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = .white
        datePicker.layer.cornerRadius = 10
        datePicker.minimumDate = Date().dateByAddingDays(-7)
        datePicker.maximumDate = Date().dateByAddingDays(7)
        datePicker.setDate(Date(), animated: true)
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: .valueChanged)
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        newDate = .custom(sender.date)
    }
}
