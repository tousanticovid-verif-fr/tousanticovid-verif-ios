//
//  CertMotorRulesRepository.swift
//  Anticovid Verify
//
//

import Foundation
import CoreData
import OSLog
import CertLogic

enum RuleIdentifierRegex {
    private static let ruleIdentifierAnyArgumentsRegex = "[A-Za-z0-9_]*-[A-Za-z0-9_]{4}-[A-Za-z0-9_]*"

    case anyArguments
    case arguments(arg1: String, arg2: String)
    case firstArgumentAny(_ arg: String)
    case secondArgumentAny(_ arg: String)
    
    var toString: String {
        get {
            switch self {
            case .anyArguments:
                return Self.ruleIdentifierAnyArgumentsRegex
            case .arguments(let arg1, let arg2):
                return "[A-Za-z0-9_]*-\(arg1)\(arg2)-[A-Za-z0-9_]*"
            case .firstArgumentAny(let arg):
                return "[A-Za-z0-9_]*-\(arg)[A-Za-z0-9_]{2}-[A-Za-z0-9_]*"
            case .secondArgumentAny(let arg):
                return "[A-Za-z0-9_]*-[A-Za-z0-9_]{2}\(arg)-[A-Za-z0-9_]*"
            }
        }
    }
}

class CertLogicMotorRepository {
    
    static let shared = CertLogicMotorRepository()
    
    let bddConfig: BDDConfig
    
    init(bddConfig: BDDConfig = .shared) {
        self.bddConfig = bddConfig
    }
    
    // MARK: - ValueSets
    
    public func deleteValueSets() throws {
        let context = self.bddConfig.getMotorContext()
        
        try context.fetch(CertLogicValueSetMotor.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try context.fetch(CertLogicValueSetIdentifier.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try context.save()
    }
    
    func delete(valueSetHashes: [String]) throws {
        let context = self.bddConfig.getMotorContext()
        let ruleRequest = CertLogicValueSetMotor.fetchRequest()
        let ruleIdRequest = CertLogicValueSetIdentifier.fetchRequest()
        let predicate = NSPredicate(format: "valueSetHash IN %@", valueSetHashes)
        ruleIdRequest.predicate = predicate
        ruleRequest.predicate = predicate
        
        try context.fetch(ruleIdRequest).forEach {
            context.delete($0)
        }
        try context.fetch(ruleRequest).forEach {
            context.delete($0)
        }
        try context.save()
    }
    
    func savedValueSetBases() -> [ValueSetBaseResponse] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicValueSetIdentifier> = CertLogicValueSetIdentifier.fetchRequest()

        do {
            return try context.fetch(fetchRequest).map { $0.mapToValueSetBaseResponse() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules for country: \(error)")
            return []
        }
    }
    
    func savedValueSets() -> [ValueSet] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicValueSetMotor> = CertLogicValueSetMotor.fetchRequest()

        do {
            return try context.fetch(fetchRequest).map { try $0.toCertLogicValueSet() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules for country: \(error)")
            return []
        }
    }
    
    @discardableResult
    public func save(valueSets: [CertLogic.ValueSet]) throws -> [CertLogicValueSetMotor] {
        let context = self.bddConfig.getMotorContext()
        
        let certRules: [CertLogicValueSetMotor] = try valueSets.map {
            let valueSetEntity = CertLogicValueSetMotor(context: context)
            try valueSetEntity.initialize(valueSet: $0)
            return valueSetEntity
        }
        
        try context.save()
        os_log("%@",type: .error, "Did save valueSets")

        return certRules
    }
    
    @discardableResult
    func save(valueSetBases: [ValueSetBaseResponse]) throws -> [CertLogicValueSetIdentifier] {
        let context = self.bddConfig.getMotorContext()
        let valueSetIds = valueSetBases.map { CertLogicValueSetIdentifier(context: context, valueSetBase: $0) }
                
        try context.save()
        return valueSetIds
    }
    
    public func getValueSets() -> [CertLogic.ValueSet] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicValueSetMotor> = CertLogicValueSetMotor.fetchRequest()
        do {
            return try context.fetch(fetchRequest).map { try $0.toCertLogicValueSet() }
        } catch {
            os_log("%@",type: .error, "Could not fetch valueSets: \(error)")
            return []
        }
    }
    
    // MARK: - Rules
    func deleteRules() throws {
        let context = self.bddConfig.getMotorContext()
        
        try context.fetch(CertLogicRulesMotor.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try context.fetch(CertLogicRuleIdentifier.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try context.save()
    }
    
    /**
     Delete CertLogicRulesMotor (rule with detailed information) and CertLogicRuleIdentifier (rule with basic information) objects whose id are the ones passed as argument.
     */
    func delete(ruleHashes: [String]) throws {
        let context = self.bddConfig.getMotorContext()
        
        try ruleHashes.forEach {
            try delete(ruleHashes: $0, context: context)
        }
        
        try context.save()
    }
    
    func savedRuleBases() -> [RuleBaseResponse] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRuleIdentifier> = CertLogicRuleIdentifier.fetchRequest()

        do {
            return try context.fetch(fetchRequest).map { $0.mapToRuleIdResponse() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules for country: \(error)")
            return []
        }
    }
    
    func savedRules() -> [Rule] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRulesMotor> = CertLogicRulesMotor.fetchRequest()

        do {
            return try context.fetch(fetchRequest).map { try $0.toCertLogicRule() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules for country: \(error)")
            return []
        }
    }
    
    @discardableResult
    func save(rules: [CertLogic.Rule]) throws -> [CertLogicRulesMotor] {
        let context = self.bddConfig.getMotorContext()
        
        let certRules: [CertLogicRulesMotor] = try rules.map {
            let certRuleEntity = CertLogicRulesMotor(context: context)
            try certRuleEntity.initialize(rule: $0)
            return certRuleEntity
        }
        
        try context.save()
        os_log("%@",type: .error, "Did save rules")
        
        return certRules
    }
    
    @discardableResult
    func save(ruleBases: [RuleBaseResponse]) throws -> [CertLogicRuleIdentifier] {
        let context = self.bddConfig.getMotorContext()
        let cerRuleIds = ruleBases.map { CertLogicRuleIdentifier(context: context, ruleId: $0) }
        
        try context.save()
        
        return cerRuleIds
    }
    
    /**
        Get rules
     */
    public func getRules() -> [CertLogic.Rule] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRulesMotor> = CertLogicRulesMotor.fetchRequest()
        
        do {
            return try context.fetch(fetchRequest).map { try $0.toCertLogicRule() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules: \(error)")
            return []
        }
    }
    
    /**
        Get specific rules for France (identifier of type XX-departureCountry/destinationCountry-XXXX)
     */
    func getRules(forDepartureCountry departureCountry: String,
                  destinationCountry: String, region: String? = nil) -> [CertLogic.Rule] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRulesMotor> = CertLogicRulesMotor.fetchRequest()
        let predicate = makePredicateForRuleWith(argument1: departureCountry, argument2: destinationCountry, region: region)

        fetchRequest.predicate = predicate
        do {
            return try context.fetch(fetchRequest).map { try $0.toCertLogicRule() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules for country: \(error)")
            return []
        }
    }
    
    // get eu rules
    func getRules(for countryCode: String, region: String? = nil) -> [CertLogic.Rule] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRulesMotor> = CertLogicRulesMotor.fetchRequest()
        let predicate = makePredicateForRuleWith(argument1: countryCode, region: region)
        
        fetchRequest.predicate = predicate
        do {
            return try context.fetch(fetchRequest).map { try $0.toCertLogicRule() }
        } catch {
            os_log("%@",type: .error, "Could not fetch rules for country: \(error)")
            return []
        }
    }
    
    func departureCountriesFromSavedRules() throws -> [String] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRuleIdentifier> = CertLogicRuleIdentifier.fetchRequest()
        let predicate = RuleIdentifierRegex.anyArguments.toString
        fetchRequest.predicate = NSPredicate(format: "identifier MATCHES[c] %@", predicate)
        
        let countryCodes = try context.fetch(fetchRequest)
            .compactMap { $0.departureCountryCode }
        
        return Array(Set(countryCodes))
    }
    
    func destinationCountriesFromSavedRules() throws -> [String] {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<CertLogicRuleIdentifier> = CertLogicRuleIdentifier.fetchRequest()
        let predicate = RuleIdentifierRegex.anyArguments.toString
        let predicate2 = RuleIdentifierRegex.firstArgumentAny("").toString

        fetchRequest.predicate = NSPredicate(format: "identifier MATCHES[c] %@ OR identifier MATCHES[c] %@", predicate, predicate2)
                
        let countryCodes = try context.fetch(fetchRequest)
            .compactMap { $0.destinationCountryCode }
        
        return Array(Set(countryCodes))
    }
}

// MARK: Privates funcs

private extension CertLogicMotorRepository {
    func makePredicateForRuleWith(argument1: String, argument2: String? = nil, region: String? = nil) -> NSPredicate {
        let query = RuleIdentifierRegex.arguments(arg1: argument1, arg2: argument2 ?? "")
        let identifierPredicate = NSPredicate(format: "identifier MATCHES[c] %@", query.toString)
        
        var regionPredicate: NSPredicate
        if let region = region {
            regionPredicate = NSPredicate(format: "region ==[c] %@", region)
        } else {
            regionPredicate = NSPredicate(format: "region == nil")
        }
        
        return NSCompoundPredicate(andPredicateWithSubpredicates: [regionPredicate,
                                                                   identifierPredicate])
    }
    
    // this function does not save context
    func delete(ruleHashes: String, context: NSManagedObjectContext) throws {
        let ruleRequest = CertLogicRulesMotor.fetchRequest()
        let ruleIdRequest = CertLogicRuleIdentifier.fetchRequest()
        let basePredicate = NSPredicate(format: "ruleHash = %@",
                                        ruleHashes)
        let detailsPredicate = NSPredicate(format: "hashStringValue = %@",
                                           ruleHashes)
        ruleIdRequest.predicate = basePredicate
        ruleRequest.predicate = detailsPredicate
        
        try context.fetch(ruleIdRequest).forEach {
            context.delete($0)
        }
        try context.fetch(ruleRequest).forEach {
            context.delete($0)
        }
    }
}
