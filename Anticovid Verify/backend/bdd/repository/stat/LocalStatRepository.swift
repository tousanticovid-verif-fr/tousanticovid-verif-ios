//
//  LocalStatRepository.swift
//  Anticovid Verify
//
//

import Foundation
import CoreData
import OSLog

class LocalStatRepository {
    
    static let shared = LocalStatRepository()
    
    let bddConfig: BDDConfig
    let confDataManager: ConfDataManager
    
    init(bddConfig: BDDConfig = .shared, confDataManager: ConfDataManager = .shared) {
        self.bddConfig = bddConfig
        self.confDataManager = confDataManager
    }

    func removeOldData() throws {
        let date = Date() - Double(confDataManager.duplicateBackupTime * 60)
        
        let context = self.bddConfig.getStatContext()
        let fetchRequest: NSFetchRequest<LocalCertStatistic> = LocalCertStatistic.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "(date <= %@)", date as NSDate)
        
        let result = try context.fetch(fetchRequest) 
        result.forEach { context.delete($0) }
        
        try context.save()
    }

    func deleteAllStatistics() -> Bool {
        let context = self.bddConfig.getStatContext()

        try? context.fetch(LocalCertStatistic.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try? context.fetch(LocalStatistic.fetchRequest()).forEach {
            context.delete($0)
        }
        
        do {
            try context.save()
            return true
        } catch {
            os_log("%@",type: .error, "Could not delete online statistic: \(error)")
            return false
        }
    }
    
    func getStatistics() -> [LocalStatistic] {
        let context = self.bddConfig.getStatContext()
        let fetchRequest: NSFetchRequest<LocalStatistic> = LocalStatistic.fetchRequest()
        do {
            return try context.fetch(fetchRequest)
        } catch {
            os_log("%@",type: .error, "Could not fetch local statistics: \(error)")
            return []
        }
    }
    
    func write(stat: LocalStatData) -> Bool {
        let context = self.bddConfig.getStatContext()
        let date = Date()
        
        do {
            let fetchRequest: NSFetchRequest<LocalStatistic> = LocalStatistic.fetchRequest()
            let localStatistics = try? context.fetch(fetchRequest)
            
            let result = localStatistics?.filter { Calendar.current.isDate(date, inSameDayAs: $0.date!) }
            if result != nil, let localStatisticEntity = result?.first {
                addOrUpdateInformationForLocalStatisticEntity(stat: stat, date: date, context: context, localStatisticEntity: localStatisticEntity)
            } else {
                let localStatisticEntity = LocalStatistic(context: context)
                localStatisticEntity.initialize(date: date)
                addOrUpdateInformationForLocalStatisticEntity(stat: stat, date: date, context: context, localStatisticEntity: localStatisticEntity)
            }
            
            try context.save()
            return true
        } catch {
            os_log("%@",type: .error, "Could not write local statistics: \(error)")
            return false
        }
    }
    
    func certificateAlreadyScan(idHash: String) -> LocalCertStatistic? {
        let context = self.bddConfig.getStatContext()
        let fetchRequest: NSFetchRequest<LocalCertStatistic> = LocalCertStatistic.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "idHash LIKE %@", idHash)
        
        let localCertStatistic = try? context.fetch(fetchRequest)
        return localCertStatistic?.first
    }
    
    private func addOrUpdateInformationForLocalStatisticEntity(stat: LocalStatData, date: Date, context: NSManagedObjectContext, localStatisticEntity: LocalStatistic) {
        if let certificateAdminState = stat.certificateAdminState, (certificateAdminState == .double || certificateAdminState == .blackListAndDouble) {
            localStatisticEntity.double += 1
            certificateAlreadyScan(idHash: stat.identifiant)?.date = date
        } else {
            switch stat.typeValidation {
            case .valid:
                localStatisticEntity.valid += 1
            case .invalid:
                localStatisticEntity.invalid += 1
            case .to_decide:
                localStatisticEntity.toDecide += 1
            }

            if !self.confDataManager.duplicateGestureActivation { return }
            
            let localCertStatistic = LocalCertStatistic(context: context)
            localCertStatistic.date = date
            localCertStatistic.idHash = stat.identifiant
        }
    }
}
