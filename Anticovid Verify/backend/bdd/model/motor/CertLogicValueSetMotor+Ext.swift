//
//  CertLogicValueSet+Ext.swift
//  Anticovid Verify
//
//

import Foundation
import CertLogic
import SwiftyJSON

extension CertLogicValueSetMotor {

    func initialize(valueSet: CertLogic.ValueSet) throws {
        self.valueSetId = valueSet.valueSetId
        self.valueSetDate = valueSet.valueSetDate
        self.valueSetValues = try JSONEncoder().encode(valueSet.valueSetValues)
        self.valueSetHash =  valueSet.hash
    }
    
    func toCertLogicValueSet() throws -> CertLogic.ValueSet {
        let valueSet = CertLogic.ValueSet(
            valueSetId: valueSetId ?? "",
            valueSetDate: valueSetDate ?? "",
            valueSetValues: try JSONDecoder().decode(Dictionary<String, ValueSetItem>.self, from: valueSetValues!))
        
        valueSet.hash = valueSetHash ?? ""
        return valueSet
    }
}

extension Sequence where Iterator.Element : CertLogic.ValueSet {

    public func toDictionnaryForExternalParameters() -> Dictionary<String, [String]> {
      var returnValue = Dictionary<String, [String]>()
        self.forEach { valueSet in
          returnValue[valueSet.valueSetId] = Array(valueSet.valueSetValues.keys)
      }
      return returnValue
    }
}
