//
//  ServiceRouter.swift
//  Anticovid Verify
//
//

import Foundation
import INCommunication
import os.log
import UIKit

enum ServiceRouter {
    case updateCertificates
    case checkMinVersion
    case statistic([String])
    case ruleIds
    case rulesDetails(ruleHashes: [String])
    case valueSetDetails(valueSetIds: [String])
    case valueSetIds
    case country
    case blacklistDCCHashes(startIndex: Int32)
    case blacklist2DDOCHashes(startIndex: Int32)
}

extension ServiceRouter: INURLRequestConvertible {

    var method: INHTTPMethod {
        switch self {
        case .updateCertificates, .valueSetIds, .checkMinVersion, .ruleIds, .country, .blacklistDCCHashes, .blacklist2DDOCHashes:
            return .get
        case .statistic, .rulesDetails, .valueSetDetails:
            return .post
        }
    }

    var queryParameters: [String: String]? {
        return nil
    }

    var queryParameter: String? {
        return nil
    }

    var path: String {
        switch self {
        case .blacklistDCCHashes(let index):
            return "api/client/configuration/blacklist/tacv/dcc/\(String(index))"
        case .blacklist2DDOCHashes(let index):
            return "api/client/configuration/blacklist/tacv/2ddoc/\(String(index))"
        case .ruleIds:
            return "api/client/configuration/rules/tacv"
        case .checkMinVersion:
            return "api/client/configuration/configuration/tacv"
        case .updateCertificates:
            return "api/client/configuration/synchronisation/tacv"
        case .statistic:
            return "api/client/configuration/synchronisation"
        case .rulesDetails:
            return "api/client/configuration/rules/hashes/tacv"
        case .valueSetDetails:
            return "api/client/configuration/valuesets/hashes/tacv"
        case .valueSetIds:
            return "api/client/configuration/valuesets/tacv"
        case .country:
            return "api/client/configuration/countries/tacv"
        }
    }

    var parameters: [String: Any]? {
        return nil
    }

    var headers: [String: String]? {
        
        let appBuildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        let currentOSVersion = UIDevice.current.systemVersion

        return [
            "Authorization": "Bearer \(ConstConf.tokenLite)",
            "Content-Type": "application/json; charset=utf-8",
            "app_version": "ios_\(appBuildVersion ?? "")",
            "os_version": "ios_\(currentOSVersion)",
            "app_mode": PremiumManager.shared.isPremium ? "psot" : "public"
        ]
    }
    
    var body: Data? {
        switch self {
        case .statistic(let body), .rulesDetails(let body), .valueSetDetails(let body):
            return try? JSONSerialization.data(withJSONObject: body, options: [])
        default: return nil
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        var url = URL(string: ConstConf.urlWs ?? "")
        url = url?.appendingPathComponent(path)
        
        if let queryParams = queryParameters, let currentUrl = url {
            let queryParamsStr = queryParams.map { (key: String, value: String)  in
                "\(key)=\(value)"
            }.joined(separator: "&")
            if let escapedString = queryParamsStr.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed){
                let urlResult = currentUrl.absoluteString + "?" + escapedString
                url = URL(string:urlResult)
            }

        }
        
        if let queryParams = queryParameter, let currentUrl = url {
            url = URL(string: "\(currentUrl.absoluteURL)/\(queryParams)")
        }
        
        os_log("%@",type: .debug, url?.debugDescription ?? "")
        var request = URLRequest(url: url!)
        request.httpMethod = method.rawValue
        request.httpBody = body
        request.allHTTPHeaderFields = headers
        return try INJsonEncoding.default.encode(request, with: parameters)
    }
}

extension ServiceRouter {

    func getPinningCerts() -> [SecCertificate] {
        return []
    }
}

