//
//  Field.swift
//  Anticovid Verify
//
//

import Foundation

public struct FieldDTO: Codable {
    var name: String
    var label: String
    var value: String
}
