//
//  Error.swift
//  Anticovid Verify
//
//

import Foundation

public struct ErrorDTO: Codable {
    
    var message: String
}
