//
//  AppConf.swift
//  Anticovid Verify
//
//

import Foundation

// MARK: - Key
struct AppConf: Codable {
    let about: About
}

// MARK: - About
struct About: Codable {
    let iosConfiguration: IosConfiguration
}

// MARK: - IosConfiguration
struct IosConfiguration: Codable {
    let lastMajorVersion, lastMinorVersion: String
}
