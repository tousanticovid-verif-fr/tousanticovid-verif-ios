//
//  ExamptedEntry.swift
//
//

import Foundation
import SwiftyJSON

struct ExamptedEntry: HCertEntry {
    
    var info: [InfoSection] {
        []
    }
    
    var walletInfo: [InfoSection] {
        []
    }
    
    var validityFailures: [String] {
        return [String]()
    }
    
    enum Fields: String {
        case diseaseTargeted = "tg"
        case exemptionStatus = "es"
        case dateValidFrom = "df"
        case dateValidUntil = "du"
        case countryCode = "co"
        case issuer = "is"
        case uvci = "ci"
    }
    
    init?(body: JSON) {
        guard
            let diseaseTargeted = body[Fields.diseaseTargeted.rawValue].string,
            let dateValidFromTimeStr = body[Fields.dateValidFrom.rawValue].string,
            let dateValidFrom = Date(dateString: dateValidFromTimeStr),
            let dateValidUntilTimeStr = body[Fields.dateValidUntil.rawValue].string,
            let dateValidUntil = Date(dateString: dateValidUntilTimeStr),
            let countryCode = body[Fields.countryCode.rawValue].string,
            let issuer = body[Fields.issuer.rawValue].string,
            let uvci = body[Fields.uvci.rawValue].string
        else {
            return nil
        }
        self.diseaseTargeted = diseaseTargeted
        self.dateValidFrom = dateValidFrom
        self.dateValidUntil = dateValidUntil
        self.countryCode = countryCode
        self.issuer = issuer
        self.uvci = uvci
        self.typeAddon = ""
    }
    
    var diseaseTargeted: String
    var dateValidFrom: Date
    var dateValidUntil: Date
    var countryCode: String
    var issuer: String
    var uvci: String
    var typeAddon: String
}
