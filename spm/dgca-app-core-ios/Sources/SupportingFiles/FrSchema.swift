/*-
 * ---license-start
 * eu-digital-green-certificates / dgca-app-core-ios
 * ---
 * Copyright (C) 2021 T-Systems International GmbH and all other contributors
 * ---
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ---license-end
 */
//
//  EuDgcSchema.swift
//
//
//  Created by Yannick Spreen on 4/20/21.
//
//  https://raw.githubusercontent.com/ehn-digital-green-development/ehn-dgc-schema/main/DGC.combined-schema.json
//

import Foundation

// swiftlint:disable line_length
public let frSchemaV1 = """
{
   "$schema":"https://json-schema.org/draft/2020-12/schema",
   "$id":"https://id.uvci.eu/DGC.combined-schema.json",
   "title":"EU DGC",
   "description":"EU Digital Green Certificate",
   "$comment":"Schema version 1.x",
   "required":[
      "nam",
      "dob"
   ],
   "type":"object",
   "properties":{
      "nam":{
         "description":"Surname(s), given name(s) - in that order",
         "$ref":"#/$defs/person_name"
      },
      "dob":{
         "title":"Date of birth",
         "description":"Date of Birth of the person addressed in the DGC. ISO 8601 date format restricted to range 1900-2099",
         "type":[
            "null",
            "string"
         ],
         "examples":[
            "1979-04-14",
            "1979-04",
            "1979",
            "",
            "1979-04-14T00:00:00"
         ]
      },
      "ex":{
         "description":"Exampted Group",
         "type":[
            "null",
            "object"
         ],
         "items":{
            "$ref":"#/$defs/exampted_entry"
         }
      },
      "a":{
         "description":"Activity Group",
         "type":[
            "null",
            "object"
         ],
         "items":{
            "$ref":"#/$defs/activity_entry"
         }
      }
   },
   "$defs":{
      "dose_posint":{
         "description":"Dose Number / Total doses in Series: positive integer, range: [1,9]",
         "type":[
            "null",
            "integer"
         ]
      },
      "country_vt":{
         "description":"Country of Vaccination / Test, ISO 3166 where possible",
         "type":[
            "null",
            "string"
         ]
      },
      "issuer":{
         "description":"Certificate Issuer",
         "type":[
            "null",
            "string"
         ]
      },
      "person_name":{
         "description":"Person name: Surname(s), given name(s) - in that order",
         "required":[
            "fnt"
         ],
         "type":"object",
         "properties":{
            "fn":{
               "title":"Family name",
               "description":"The family or primary name(s) of the person addressed in the certificate",
               "type":[
                  "null",
                  "string"
               ],
               "examples":[
                  "d'Červenková Panklová"
               ]
            },
            "fnt":{
               "title":"Standardised family name",
               "description":"The family name(s) of the person transliterated",
               "type":[
                  "null",
                  "string"
               ],
               "examples":[
                  "DCERVENKOVA<PANKLOVA"
               ]
            },
            "gn":{
               "title":"Given name",
               "description":"The given name(s) of the person addressed in the certificate",
               "type":[
                  "null",
                  "string"
               ],
               "examples":[
                  "Jiřina-Maria Alena"
               ]
            },
            "gnt":{
               "title":"Standardised given name",
               "description":"The given name(s) of the person transliterated",
               "type":[
                  "null",
                  "string"
               ],
               "examples":[
                  "JIRINA<MARIA<ALENA"
               ]
            }
         }
      },
      "certificate_id":{
         "description":"Certificate Identifier, format as per UVCI: Annex 2 in  https://ec.europa.eu/health/sites/health/files/ehealth/docs/vaccination-proof_interoperability-guidelines_en.pdf",
         "type":[
            "null",
            "string"
         ]
      },
      "examption_status":{
         "description":"Status of examption dcc",
         "type":[
            "null",
            "string"
         ]
      },
      "date_valid_from":{
         "description":"Start date of the examption dcc",
         "type":[
            "null",
            "string"
         ]
      },
      "date_valid_until":{
         "description":"End date of the examption dcc",
         "type":[
            "null",
            "string"
         ]
      },
      "exampted_entry":{
         "description":"Exampted Entry",
         "required":[
            "tg",
            "es",
            "df",
            "du",
            "co",
            "is",
            "ci"
         ],
         "type":[
            "null",
            "object"
         ],
         "properties":{
            "tg":{
               "description":"disease or agent targeted",
               "$ref":"#/$defs/disease-agent-targeted"
            },
            "es":{
               "description":"examption status",
               "$ref":"#/$defs/examption_status"
            },
            "df":{
               "description":"date valid from",
               "$ref":"#/$defs/date_valid_from"
            },
            "du":{
               "description":"date valid until",
               "$ref":"#/$defs/date_valid_until"
            },
            "co":{
               "description":"Country of Vaccination",
               "$ref":"#/$defs/country_vt"
            },
            "is":{
               "description":"Certificate Issuer",
               "$ref":"#/$defs/issuer"
            },
            "ci":{
               "description":"Unique Certificate Identifier: UVCI",
               "$ref":"#/$defs/certificate_id"
            }
         }
      },
      "activity_entry":{
         "description":"Acitivty Entry",
         "required":[
            "ci"
         ],
         "type":[
            "null",
            "object"
         ],
         "properties":{
            "ci":{
               "description":"Unique Certificate Identifier: UVCI",
               "$ref":"#/$defs/certificate_id"
            }
         }
      },
      "disease-agent-targeted":{
         "description":"EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.1",
         "type":[
            "null",
            "string"
         ],
         "valueset-uri":"valuesets/disease-agent-targeted.json"
      },
      "vaccine-prophylaxis":{
         "description":"EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.2",
         "type":[
            "null",
            "string"
         ],
         "valueset-uri":"valuesets/vaccine-prophylaxis.json"
      },
      "vaccine-medicinal-product":{
         "description":"EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.3",
         "type":[
            "null",
            "string"
         ],
         "valueset-uri":"valuesets/vaccine-medicinal-product.json"
      },
      "vaccine-mah-manf":{
         "description":"EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.4",
         "type":[
            "null",
            "string"
         ],
         "valueset-uri":"valuesets/vaccine-mah-manf.json"
      },
      "test-manf":{
         "description":"EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.8",
         "type":[
            "null",
            "string"
         ],
         "valueset-uri":"valuesets/test-manf.json"
      },
      "test-result":{
         "description":"EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.9",
         "type":[
            "null",
            "string"
         ],
         "valueset-uri":"valuesets/test-results.json"
      }
   }
}
"""
// swiftlint:enable line_length

