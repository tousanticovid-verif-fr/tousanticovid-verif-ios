//
//  RulesServiceTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import Anticovid_Verify

class RulesServiceTest: XCTestCase {
    
    let mockBDDConfig = MockBDDConfig()
    
    private var rulesService: RulesService!
    
    private var rulesMotorRepository: RulesMotorRepositoryMock!
    
    override func setUp() {
        super.setUp()
        
        rulesMotorRepository = RulesMotorRepositoryMock()
        rulesService = RulesService(
            rulesMotorRepository: rulesMotorRepository
        )
    }
    
    func testGetRulesWhenRulesMotorIsNil() {
        _ = self.rulesService.getRules()
        XCTAssertTrue(rulesMotorRepository.callMotorRules)
    }
    
    //MARK: - IsVaccineProphylaxis
    func testIsVaccineProphylaxisWhenVaccineProphylaxisExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.vaccineProphylaxisList = ["Prophylaxis": "azertyuiop"]
        
        XCTAssertTrue(rulesService.isVaccineProphylaxis(prophylacticAgent: "Prophylaxis"))
    }
    
    func testIsVaccineProphylaxisWhenVaccineProphylaxisNotExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.vaccineProphylaxisList = [:]
        
        XCTAssertFalse(rulesService.isVaccineProphylaxis(prophylacticAgent: "Prophylaxis"))
    }
    
    //MARK: - IsVaccineMedicinalProduct
    func testisVaccineMedicinalProductWhenVaccineMedicinalProductExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.vaccineMedicalProductList = ["product": "azertyuiop"]
        
        XCTAssertTrue(rulesService.isVaccineMedicinalProduct(vaccineName: "product"))
    }
    
    func testisVaccineMedicinalProductWhenVaccineMedicinalProductNotExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.vaccineMedicalProductList = [:]
        
        XCTAssertFalse(rulesService.isVaccineMedicinalProduct(vaccineName: "product"))
    }
    
    //MARK: - vaccineMahManfContains
    func testVaccineMahManfContainsWhenManufacturerExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.vaccineManufacturerList = ["manu": "azertyuiop"]
        
        XCTAssertTrue(rulesService.vaccineMahManfContains(manufacturer: "manu"))
    }
    
    func testVaccineMahManfContainsWhenManufacturerNotExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.vaccineManufacturerList = [:]
        
        XCTAssertFalse(rulesService.vaccineMahManfContains(manufacturer: "azertyuiop"))
    }
    
    //MARK: - testManfContains
    func testTestManfContainsWhenManufacturerExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testManufacturerList = ["manu": "azertyuiop"]
        
        XCTAssertTrue(rulesService.testManfContains(manufacturer: "manu"))
    }
    
    func testTestManfContainsWhenManufacturerNotExist() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testManufacturerList = [:]
        
        XCTAssertFalse(rulesService.testManfContains(manufacturer: "azertyuiop"))
    }
    
    //MARK: - isTest
    func testIsTestReturnTrueWhenTestIsPCR() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testPcrList = ["TEST_PCR_1": "azertyuiop"]
        
        XCTAssertTrue(rulesService.isTest(loinc: "TEST_PCR_1"))
    }
    
    func testIsTestReturnTrueWhenTestIsAntigenic() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testAntigenicList = ["TEST_ANTIGENIC_1": "azertyuiop"]
        
        XCTAssertTrue(rulesService.isTest(loinc: "TEST_ANTIGENIC_1"))
    }
    
    func testIsTestReturnTrueWhenTestIsNotAntigenicAndNotPCR() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testAntigenicList = [:]
        self.rulesMotorRepository.rulesMotor?.testPcrList = [:]
        
        XCTAssertFalse(rulesService.isTest(loinc: "NOT_A_TEST_1"))
    }
    
    //MARK: - isPCR
    func testIsPCRReturnTrueWhenTestIsPCR() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testPcrList = ["TEST_PCR_1": "azertyuiop"]
        
        XCTAssertTrue(rulesService.isPcr(loinc: "TEST_PCR_1"))
    }
    
    func testIsPCRReturnFalseWhenTestIsPCR() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testPcrList = [:]
        
        XCTAssertFalse(rulesService.isPcr(loinc: "NOT_A_PCR_TEST_1"))
    }
    
    
    //MARK: - isAntigenic
    func testIsAntigenicReturnTrueWhenTestIsAntigenic() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testAntigenicList = ["TEST_ANTIGENIC_1": "azertyuiop"]
        
        XCTAssertTrue(rulesService.isAntigenic(loinc: "TEST_ANTIGENIC_1"))
    }
    
    func testIsAntigenicReturnTrueWhenTestIsNotAntigenic() {
        self.rulesMotorRepository.rulesMotor = RulesMotor(context: mockBDDConfig.getMotorContext())
        self.rulesMotorRepository.rulesMotor?.testPcrList = [:]
        
        XCTAssertFalse(rulesService.isAntigenic(loinc: "NOT_A_ANTIGENIC_TEST_1"))
    }
}

class RulesMotorRepositoryMock: RulesMotorRepository {
    
    var callMotorRules: Bool = false
    var rulesMotor: RulesMotor? = nil
    override func getMotorRules() -> RulesMotor? {
        callMotorRules = true
        return rulesMotor
    }
    
    var deleteCalled: Bool = false
    override func deletePreviousRules() -> Bool {
        deleteCalled = true
        return deleteCalled
    }
    
    override func save(notGreenCountries: [String], validity: Validity,
                       specificValueList: SpecificValueList) throws -> RulesMotor {
        let rules = RulesMotor(context: MockBDDConfig().getMotorContext())
        try rules.initialize(notGreenCountries: notGreenCountries, validity: validity, specificValueList: specificValueList)
        return rules
    }
}
