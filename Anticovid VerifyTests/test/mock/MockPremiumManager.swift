//
//  MockPremiumManager.swift
//  Anticovid VerifyTests
//
//

import Foundation
@testable import Anticovid_Verify

class MockPremiumManager: PremiumManager {
    
    var mockModeSelected: ModeSelected = .tacVerif
    
    func getModeSelect() -> ModeSelected {
        return mockModeSelected
    }
}
