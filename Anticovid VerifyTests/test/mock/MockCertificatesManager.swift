//
//  MockCertificatesManager.swift
//  Anticovid VerifyTests
//
//

import Foundation
@testable import Anticovid_Verify

class MockCertificatesManager: CertificatesManager {
    
    var remingStep: RemindStep = .none

    func needRemind() -> RemindStep {
        return remingStep
    }
}
