//
//  MockSyncManager.swift
//  Anticovid VerifyTests
//
//

import Foundation
@testable import Anticovid_Verify

class MockSyncManager: SyncManager {
    
    var remingStep: RemindStep = .none

    override func actualAppSyncRemindStep() -> RemindStep {
        return remingStep
    }
}
