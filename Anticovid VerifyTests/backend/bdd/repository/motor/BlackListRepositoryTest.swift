//
//  BlackListRepositoryTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
import XCTest
@testable import Anticovid_Verify

class BlackListRepositoryTest: XCTestCase {
    
    let mockBDDConfig = MockBDDConfig()
    
    var context: NSManagedObjectContext!

    var blackListRepository: BlackListRepository! = nil
    
    override func setUp() {
        super.setUp()
        self.context = mockBDDConfig.getMotorContext()
        try? self.context.save()
        
        blackListRepository = BlackListRepository(bddConfig: mockBDDConfig)
    }
    
    func testDCCBlacklisted() {
        let response: [BlacklistHashResponse] = [.init(hash: "testDCCBlacklisted", id: 0, active: nil)]
        
        XCTAssertNoThrow(try blackListRepository.save(blacklistDCCResponse: response))
        XCTAssertTrue(blackListRepository.isBlackListed(certHash: "testDCCBlacklisted", isDCC: true))
    }
    
    func test2DDOCBlacklisted() {
        let response: [BlacklistHashResponse] = [.init(hash: "test2DDOCBlacklisted", id: 0, active: nil)]

        XCTAssertNoThrow(try blackListRepository.save(blacklist2DDOCResponse: response))
        XCTAssertTrue(blackListRepository.isBlackListed(certHash: "test2DDOCBlacklisted", isDCC: false))
    }
    
    func testDCCNotBlacklisted() {
        XCTAssertFalse(blackListRepository.isBlackListed(certHash: "testDCCNOTBlacklisted", isDCC: false))
    }
    
    func test2DDOCNotBlacklisted() {
        XCTAssertFalse(blackListRepository.isBlackListed(certHash: "test2DDOCNOTBlacklisted", isDCC: false))
    }
    
    func testSaveConfiguration() {
        let blackListMessage = BlacklistMessageDetail(titleFR: "fr", detailFR: "fr_details",
                                                           titleEN: "en", detailEN: "en_details")
        let messages = BlacklistMessages(blacklistLite: blackListMessage,
                                         blacklistOT: blackListMessage,
                                         duplicate: blackListMessage)
        let config = Blacklist(duplicateActivate: false, duplicateRetentionPeriod: 5,
                                     messages: messages, exclusionQuantityLimit: 10,
                                     exclusionTime: [], refreshVersion: 0,
                                     refreshIndex: 0, refreshIndex2ddoc: 0,
                                     lastIndexBlacklist: 0, lastIndexBlacklist2ddoc: 0,
                                     standbyDelay: 0)
        
        XCTAssertNoThrow(try blackListRepository.save(configuration: config))
        XCTAssertNotNil(blackListRepository.configuration)
    }
    
    
    func testRetrieveLastIndexDCC() {
        let response = Array(0...30).map { BlacklistHashResponse(hash: "test2DDOCBlacklisted",
                                                            id: Int32($0), active: nil) }
        
        XCTAssertNoThrow(try blackListRepository.save(blacklistDCCResponse: response))
        XCTAssertEqual(blackListRepository.lastBlacklistDCCIndex(), 30)
    }
    
    func testRetrieveLastIndex2DDOC() {
        let response = Array(0...20).map { BlacklistHashResponse(hash: "test2DDOCBlacklisted",
                                                            id: Int32($0), active: nil) }

        XCTAssertNoThrow(try blackListRepository.save(blacklist2DDOCResponse: response))
        XCTAssertEqual(blackListRepository.lastBlacklist2DDOCIndex(), 20)
    }
    
    func testSaveConfigurationWithDeletingSomeBlacklists() {
        // save blacklist to delete after refesh config
        let responseDCC = Array(0...30).map { BlacklistHashResponse(hash: "test2DDOCBlacklisted",
                                                            id: Int32($0), active: nil) }
        
        XCTAssertNoThrow(try blackListRepository.save(blacklistDCCResponse: responseDCC))
        
        // save blacklist to delete after refesh config
        let response2DDOC = Array(0...40).map { BlacklistHashResponse(hash: "test2DDOCBlacklisted",
                                                            id: Int32($0), active: nil) }
        
        XCTAssertNoThrow(try blackListRepository.save(blacklist2DDOCResponse: response2DDOC))

        // save new config to refresh blacklists (delete unwanted ones)
        let blackListMessage = BlacklistMessageDetail(titleFR: "fr", detailFR: "fr_details",
                                                           titleEN: "en", detailEN: "en_details")
        let messages = BlacklistMessages(blacklistLite: blackListMessage,
                                         blacklistOT: blackListMessage,
                                         duplicate: blackListMessage)
        let config = Blacklist(duplicateActivate: false, duplicateRetentionPeriod: 5,
                                     messages: messages, exclusionQuantityLimit: 10,
                                     exclusionTime: [], refreshVersion: 1,
                                     refreshIndex: 10, refreshIndex2ddoc: 20,
                                     lastIndexBlacklist: 9, lastIndexBlacklist2ddoc: 19,
                                     standbyDelay: 0)
        
        XCTAssertNoThrow(try blackListRepository.save(configuration: config))

        // check deletion
        XCTAssertEqual(blackListRepository.lastBlacklistDCCIndex(), 9)
        XCTAssertEqual(blackListRepository.lastBlacklist2DDOCIndex(), 19)

    }
}
